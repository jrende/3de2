#version 330
in vec3 TexCoord0;
in vec3 Normal0;
in vec3 WorldPos0;
in vec3 Tangent0;
in vec3 Bitangent0;

out vec4 FragColor;

uniform struct PointLight {
    vec3 color;
    vec3 position;
    float ambientIntensity;
    float diffuseIntensity;
    float constant;
    float exp;
    float linear;
} pointLight;

uniform vec3 diffuse;
uniform vec3 ambient;
uniform vec3 specular;

uniform sampler2D gSampler;
uniform sampler2D nSampler;
uniform sampler2D sSampler;

uniform float gMatSpecularIntensity;
uniform float gSpecularExponent;

uniform vec3 eyePos;
uniform vec3 eyeDir;

vec3 getNormal() {
	vec3 normal = normalize(Normal0);
	vec3 tangent = normalize(Tangent0);
	vec3 bitangent = normalize(Bitangent0);
	vec3 bumpmapNormal = 2.0 * texture2D(nSampler, TexCoord0.st).xyz - vec3(1, 1, 1);
	return normalize(mat3(tangent, bitangent, normal) * bumpmapNormal);
}

vec4 getDiffuse(vec3 normal) {
	vec3 surfaceToLight = pointLight.position - WorldPos0;
	float distance = length(surfaceToLight);

	float brightness = max(dot(normal, normalize(surfaceToLight)), 0);
    brightness /= pointLight.constant + pointLight.linear*distance + pointLight.exp*distance*distance;

	vec4 lightColor =  vec4(pointLight.color, 1.0f);
    vec4 diffuseColor = lightColor * pointLight.diffuseIntensity;
	vec4 ambientColor = lightColor * pointLight.ambientIntensity;
	return diffuseColor * brightness + ambientColor;
}

vec4 getSpecular(vec3 normal) {
	vec3 surfaceToLight = pointLight.position - WorldPos0;

	vec3 lightReflect = normalize(reflect(-surfaceToLight, normal));
	vec3 surfaceToEye = normalize(eyePos - WorldPos0);

	float specularFactor = clamp(dot(surfaceToEye, lightReflect), 0.0, 1.0);
	float specularIntensity = texture2D(sSampler, TexCoord0.st).x * gMatSpecularIntensity;
	specularFactor = pow(specularFactor, gSpecularExponent);
	return vec4(pointLight.color, 1.0f) * specularIntensity * specularFactor;
}

void main() {
	vec3 normal = getNormal();

	vec4 fogColor = vec4(0, 0, 0, 0);
	float fogDensity = 25;
	float fogFactor = 1 - clamp(length(eyePos - WorldPos0)/fogDensity, 0.0, 1.0);
	vec4 color = texture2D(gSampler, TexCoord0.st);
	color += getSpecular(normal);
	color *= getDiffuse(normal);
	FragColor = color;
	//FragColor = mix(fogColor, color, fogFactor);
}

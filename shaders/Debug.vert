#version 330
layout (location = 0) in vec3 Position;

uniform mat4 mvpMatrix;
uniform mat4 modelMatrix;

void main(void) {
	gl_PointSize = 10.0;
	gl_Position = mvpMatrix * modelMatrix * vec4(Position, 1.0);
}
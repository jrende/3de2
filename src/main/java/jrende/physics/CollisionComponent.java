package jrende.physics;

import com.artemis.Component;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.Transform;

import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

public class CollisionComponent extends Component {
    public CollisionShape collisionShape;
    public RigidBody rigidBody;
    public float mass;

    public CollisionComponent(CollisionShape collisionShape, float mass) {
        this.collisionShape = collisionShape;
        this.mass = mass;

        Transform t = new Transform();
        t.setRotation(new Quat4f(0, 0, 0, 1));
        t.transform(new javax.vecmath.Vector3f(0, 0, 0));
        DefaultMotionState fallMotionState = new DefaultMotionState(t);
        javax.vecmath.Vector3f fallInertia = new javax.vecmath.Vector3f(0, 50, 0);
        collisionShape.calculateLocalInertia(mass, fallInertia);
        collisionShape.setLocalScaling(new javax.vecmath.Vector3f(1.0f, 1.0f, 1.0f));
        RigidBodyConstructionInfo fallRigidBodyCI = new RigidBodyConstructionInfo(
                mass, fallMotionState, collisionShape, fallInertia);
        rigidBody = new RigidBody(fallRigidBodyCI);
        rigidBody.setFriction(2);
        rigidBody.setRestitution(0.2f);
    }

    public CollisionComponent(CollisionShape collisionShape, Vector3f pos, float mass) {
        this(collisionShape, mass);
        rigidBody.translate(pos);
    }

    public void translate(javax.vecmath.Vector3f v) {
//		rigidBody.activate();
        rigidBody.translate(v);
    }

    public void translate(org.lwjgl.util.vector.Vector3f v) {
//		rigidBody.activate();
        rigidBody.translate(new Vector3f(v.x, v.y, v.z));
    }

    public void addForce(Vector3f vector3f) {
        rigidBody.activate();
        rigidBody.applyImpulse(vector3f, new Vector3f(0, 0, 0));
    }

    public void addRotForce(Vector3f torque) {
        rigidBody.activate();
        rigidBody.applyTorque(torque);
    }

    public float getMass() {
        return mass;
    }

    public void setMass(float mass) {
        this.mass = mass;
    }
}

package jrende.physics;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.systems.IntervalEntityProcessingSystem;
import com.bulletphysics.collision.broadphase.BroadphaseInterface;
import com.bulletphysics.collision.broadphase.Dbvt;
import com.bulletphysics.collision.broadphase.DbvtBroadphase;
import com.bulletphysics.collision.broadphase.Dispatcher;
import com.bulletphysics.collision.dispatch.CollisionDispatcher;
import com.bulletphysics.collision.dispatch.DefaultCollisionConfiguration;
import com.bulletphysics.dynamics.DiscreteDynamicsWorld;
import com.bulletphysics.dynamics.DynamicsWorld;
import com.bulletphysics.dynamics.constraintsolver.SequentialImpulseConstraintSolver;
import com.bulletphysics.linearmath.MotionState;
import com.bulletphysics.linearmath.Transform;
import jrende.graphics.TransformComponent;

public class PhysicsSystem extends IntervalEntityProcessingSystem {
    @Mapper
    ComponentMapper<CollisionComponent> cm;
    @Mapper
    ComponentMapper<TransformComponent> tm;

    Transform trans = new Transform();
    DynamicsWorld dynamicsWorld;
    float timeStep = 0.0f / 60.0f;

    @SuppressWarnings("unchecked")
    public PhysicsSystem() {
        super(Aspect.getAspectForAll(CollisionComponent.class,
                TransformComponent.class), 10);

        BroadphaseInterface broadphase = new DbvtBroadphase();
        DefaultCollisionConfiguration defaultConfig = new DefaultCollisionConfiguration();
        CollisionDispatcher dispatcher = new CollisionDispatcher(defaultConfig);
        SequentialImpulseConstraintSolver solver = new SequentialImpulseConstraintSolver();

        dynamicsWorld = new DiscreteDynamicsWorld(dispatcher, broadphase, solver, defaultConfig);
        dynamicsWorld.setGravity(new javax.vecmath.Vector3f(0, -10, 0));
        //dynamicsWorld.setGravity(new javax.vecmath.Vector3f(0, 0, 0));
    }

    @Override
    protected void begin() {
        dynamicsWorld.stepSimulation(timeStep, 10);
    }

    float[] mat = new float[4 * 4];

    @Override
    protected void process(Entity e) {
        CollisionComponent collisionComponent = cm.get(e);
        TransformComponent transformComponent = tm.get(e);
        if (collisionComponent.getMass() > 0.01) {
            MotionState ms = collisionComponent.rigidBody.getMotionState();
            ms.getWorldTransform(trans);
            trans.getOpenGLMatrix(mat);
            transformComponent.transform(mat);
        }
    }

    @Override
    protected void end() {
    }

    @Override
    protected void inserted(Entity e) {
        //System.out.println("Hej");
        CollisionComponent collisionComponent = cm.get(e);

        dynamicsWorld.addRigidBody(collisionComponent.rigidBody);
    }

    public void setTimeStep(float timeStep) {
        this.timeStep = timeStep;
    }

}

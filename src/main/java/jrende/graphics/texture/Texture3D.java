package jrende.graphics.texture;


import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.ARBBufferObject.*;
import static org.lwjgl.opengl.ARBVertexBufferObject.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL31.*;
import static org.lwjgl.opengl.GL32.*;
import static org.lwjgl.opengl.GL33.*;
import static org.lwjgl.opengl.EXTTextureFilterAnisotropic.*;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL12;

public class Texture3D {
	class Color {
		float r;
		float g;
		float b;
		public Color(float r, float g, float b) {
			this.r = r;
			this.g = g;
			this.b = b;
		}
	}
	List<Color> colorList;
	private final int BYTES_PER_PIXEL = 4;
	int textureID;
	private Texture3D(String filename) throws IOException{
		BufferedImage img = ImageIO.read(new File(filename));
//		IntBuffer imgData = BufferUtils.createIntBuffer(img.getWidth() * img.getHeight() * INTS_PER_PIXEL);
		int[] pixels = new int[img.getWidth() * img.getHeight()];
		img.getRGB(0, 0, img.getWidth(), img.getHeight(), pixels, 0, img.getWidth());
		
//		System.out.println("W: " + img.getWidth() + ", H: " + img.getHeight());
		
		ByteBuffer pixBuf = BufferUtils.createByteBuffer(pixels.length * BYTES_PER_PIXEL); 
		for(int i  = 0; i < pixels.length; i += 1) {
			byte a = (byte) ((pixels[i] >> 3*8) & 0xFF);
			byte r = (byte) ((pixels[i] >> 2*8) & 0xFF);
			byte g = (byte) ((pixels[i] >> 1*8) & 0xFF);
			byte b = (byte) ((pixels[i] >> 0*8) & 0xFF);
//			System.out.print("(" + r + ", " + g + ", " + b + ", " + a + "), ");
//			if((i != 0) && (i % (img.getWidth())) == 1) {
//				System.out.println("");
//			}
			pixBuf.put(b).put(g).put(r).put(a);
//			pixBuf.put(r).put(g).put(b).put(a);
		}
		pixBuf.flip();
		
		textureID = glGenTextures();
		glBindTexture(GL_TEXTURE_2D, textureID); //Bind texture ID
        
        initPara();
        
        glTexImage2D(GL_TEXTURE_2D, 0, 4, img.getWidth(), img.getHeight(), 0, GL12.GL_BGRA, GL_UNSIGNED_BYTE, pixBuf);
//		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, img.getWidth() - 1, img.getHeight() - 1, GL12.GL_BGRA, GL_BYTE, pixBuf);
	}
	
	//Creates a 1x1 of a single color.
	public Texture3D() {
		colorList = new ArrayList<Color>();
	}
	public void addColor(float r, float g, float b) {
		colorList.add(new Color(r, g, b));
	}
	public void createTexture() {
		if(!IsPowerOfTwo(colorList.size())) {
			System.err.println("3d-texture not power of two.");
		}
		FloatBuffer colorBuf = BufferUtils.createFloatBuffer(3 * colorList.size());
		for(int i = 0;i < colorList.size(); i++) {
			colorBuf.put(colorList.get(i).r).put(colorList.get(i).g).put(colorList.get(i).b);
			
			System.out.println(colorList.get(i).r + ", " + colorList.get(i).g + ", " + colorList.get(i).b);
		}
		colorBuf.flip();
		
		textureID = glGenTextures();
		glBindTexture(GL_TEXTURE_3D, textureID); //Bind texture ID

		initPara();
		
		glTexImage3D(GL_TEXTURE_3D, 0, GL_RGB, 1, 1, colorList.size(), 0, GL_RGB, GL_FLOAT, colorBuf);
	}
	public void bind() {
		glActiveTexture(0);
		glBindTexture(GL_TEXTURE_3D, textureID);
	}
	
	private void initPara() {
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_REPEAT);
	}
	
	boolean IsPowerOfTwo(long x) {
	    return (x != 0) && ((x & (x - 1)) == 0);
	}

}

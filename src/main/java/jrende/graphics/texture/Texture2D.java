package jrende.graphics.texture;

import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;
import org.lwjgl.BufferUtils;
import static org.lwjgl.opengl.GL11.GL_RGBA;
import static org.lwjgl.opengl.GL12.GL_BGRA;
import static org.lwjgl.opengl.EXTTextureFilterAnisotropic.*;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL30.glGenerateMipmap;

public class Texture2D implements Texture {
    private final int BYTES_PER_PIXEL = 4;
    int textureID;

    public Texture2D(String filename) throws IOException {
        BufferedImage img = null;
        if(filename.endsWith("tga")) {
            img = (BufferedImage) TargaReader.getImage(filename);
        } else {
            try {
                img = Imaging.getBufferedImage(new File(filename));
            } catch (IOException e) {
                throw new IOException("Can't read " + filename + "!");
            } catch (ImageReadException e) {
                e.printStackTrace();
            }
        }
        if(img == null) {
            throw new IOException("Unable to parse " + filename + "!");
        }
        int[] pixels = new int[img.getWidth() * img.getHeight()];
        img.getRGB(0, 0, img.getWidth(), img.getHeight(), pixels, 0,
                img.getWidth());

        ByteBuffer pixBuf = BufferUtils.createByteBuffer(pixels.length
                * BYTES_PER_PIXEL);
        for (int i = 0; i < pixels.length; i += 1) {
            byte a = (byte) ((pixels[i] >> 3 * 8) & 0xFF);
            byte r = (byte) ((pixels[i] >> 2 * 8) & 0xFF);
            byte g = (byte) ((pixels[i] >> 1 * 8) & 0xFF);
            byte b = (byte) ((pixels[i] >> 0 * 8) & 0xFF);
            pixBuf.put(b).put(g).put(r).put(a);
        }
        pixBuf.flip();

        textureID = glGenTextures();
        glBindTexture(GL_TEXTURE_2D, textureID); // Bind texture ID

        setParameters();

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img.getWidth(), img.getHeight(), 0, GL_BGRA, GL_UNSIGNED_BYTE, pixBuf);
    }

    // Creates a 1x1 texture of a single color.
    public Texture2D(float r, float g, float b) {
        FloatBuffer colorBuf = BufferUtils.createFloatBuffer(3);
        colorBuf.put(r).put(g).put(b);
        colorBuf.flip();

        textureID = glGenTextures();
        glBindTexture(GL_TEXTURE_2D, textureID); // Bind texture ID

        setParameters();

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 1, 1, 0, GL_RGB, GL_FLOAT, colorBuf);

    }

    public void bind(int activeTexture) {
        // glActiveTexture(activeTexture);
        glBindTexture(GL_TEXTURE_2D, textureID);
    }

    @Override
    public void bind() {
        this.bind(0);
    }

    private void setParameters() {
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f);

        glGenerateMipmap(GL_TEXTURE_2D);  //Generate num_mipmaps number of mipmaps here.
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }

    public int getTexID() {
        return textureID;
    }

}

package jrende.graphics.texture;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;

public enum TextureType {
    DiffuseMap(GL_TEXTURE0, "map_Kd"),
    BumpMap(GL_TEXTURE1, "map_bump"),
    SpecularHighlightMap(GL_TEXTURE2, "map_Ns"),
    AmbientMap(0, "map_Ka"),
    SpecularColorMap(0, "map_Ks"),
    AlphaTextureMap(0, "map_d"),
    DisplacementMap(0, "disp"),
    StencilDecalTexture(0, "decal");

    int textureUnit;
    String name;
    TextureType(int textureUnit, String name) {
        this.textureUnit = textureUnit;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getTextureUnit() {
        return textureUnit;
    }

    public static TextureType getTypeFromName(String name) {
        for(TextureType type: TextureType.values()) {
            if(type.getName().equals(name)) {
                return type;
            }
        }
        return null;
    }
}

package jrende.graphics;

import jrende.graphics.g3d.light.LightComponent;
import jrende.graphics.g3d.shaders.DebugShader;
import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import java.nio.FloatBuffer;

public class DebugRenderer {

    DebugShader debugShader = new DebugShader();
    FloatBuffer lightPointBuf = BufferUtils.createFloatBuffer(4 * 4);
    Matrix4f lightPointMat = new Matrix4f();
    public void drawPoint(Vector3f pos, FloatBuffer mvpMat) {
        debugShader.useShader();

        lightPointBuf.clear();
        lightPointMat.setIdentity();
        lightPointMat.translate(pos);
        lightPointMat.store(lightPointBuf);
        lightPointBuf.flip();

        debugShader.setMVPMatrix(mvpMat);
        debugShader.setModelMatrix(lightPointBuf);
        debugShader.drawPoint();
    }

}

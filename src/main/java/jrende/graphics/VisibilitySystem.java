package jrende.graphics;

import com.artemis.Aspect;
import com.artemis.Entity;
import com.artemis.EntitySystem;
import com.artemis.utils.ImmutableBag;

public class VisibilitySystem extends EntitySystem {

    public VisibilitySystem() {
        super(Aspect.getAspectForAll(VisibilityComponent.class));
    }

    @Override
    protected void processEntities(ImmutableBag<Entity> entities) {

    }

    @Override
    protected boolean checkProcessing() {
        return true;
    }
}

package jrende.graphics;

import com.artemis.Component;
import jrende.graphics.g3d.material.Material;
import jrende.graphics.texture.TextureType;

import java.nio.FloatBuffer;

import java.nio.FloatBuffer;

public class RenderComponent extends Component {
    RenderObject renderObject;

    public RenderComponent(RenderObject renderObject) {
        this.renderObject = renderObject;
    }

    public void render() {
        renderObject.render();
    }

    public Material getMaterial() {
        return renderObject.getMaterial();
    }

    public void bindTexture(TextureType type) {
        renderObject.bindTexture(type);
    }
}

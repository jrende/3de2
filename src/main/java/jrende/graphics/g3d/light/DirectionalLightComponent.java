package jrende.graphics.g3d.light;

import org.lwjgl.util.vector.Vector3f;

public class DirectionalLightComponent extends LightComponent {
    Vector3f direction;

    public Vector3f getDirection() {
        return direction;
    }

    public void setDirection(Vector3f direction) {
        this.direction = direction;
    }

    @Override
    public void setPosition(Vector3f pos) {
        direction = pos.normalise(direction);
    }

    @Override
    public Vector3f getPosition() {
        return direction;
    }
}

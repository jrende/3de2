package jrende.graphics.g3d.light;

import org.lwjgl.util.vector.Vector3f;

public class SpotLightComponent extends LightComponent {
    Vector3f dir;
    float radius;
    float concentration;
    float constant;
    float linear;
    float exp;

    public float getConcentration() {
        return concentration;
    }

    public void setConcentration(float concentration) {
        this.concentration = concentration;
    }

    public Vector3f getDir() {
        return dir;
    }

    public void setDir(Vector3f dir) {
        this.dir = dir;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public float getConstant() {
        return constant;
    }

    public void setConstant(float constant) {
        this.constant = constant;
    }

    public float getLinear() {
        return linear;
    }

    public void setLinear(float linear) {
        this.linear = linear;
    }

    public float getExp() {
        return exp;
    }

    public void setExp(float exp) {
        this.exp = exp;
    }
}

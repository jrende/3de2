package jrende.graphics.g3d.light;

import com.artemis.Component;
import org.lwjgl.util.vector.Vector3f;

public class LightComponent extends Component {
    Vector3f color;
    Vector3f position;
    float ambientIntensity;
    float diffuseIntensity;


    public Vector3f getColor() {
        return color;
    }

    public void setColor(Vector3f color) {
        this.color = color;
    }

    public float getAmbientIntensity() {
        return ambientIntensity;
    }

    public void setAmbientIntensity(float ambientIntensity) {
        this.ambientIntensity = ambientIntensity;
    }

    public float getDiffuseIntensity() {
        return diffuseIntensity;
    }

    public void setDiffuseIntensity(float diffuseIntensity) {
        this.diffuseIntensity = diffuseIntensity;
    }

    public void setPosition(Vector3f pos) {
        position = pos;
    }

    public Vector3f getPosition() {
        return position;
    }
}

package jrende.graphics.g3d.shaders;

import jrende.graphics.g3d.light.SpotLightComponent;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL20.*;

public class SpotLightModelShader extends ModelShader<SpotLightComponent> {
    private int spotlightColID;
    private int spotlightAmbIntID;
    private int spotlightDiffIntID;
    private int spotlightLinearID;
    private int spotlightExpID;
    private int spotlightConstantID;
    private int spotlightPosID;
    private int spotlightDirID;
    private int spotlightConcentrationID;

    public SpotLightModelShader() {
        super("SpotLight");
        spotlightColID = getUniform("spotlight.color");
        spotlightPosID = getUniform("spotlight.position");
        spotlightDirID = getUniform("spotlight.direction");
        spotlightExpID = getUniform("spotlight.exp");
        spotlightLinearID = getUniform("spotlight.linear");
        spotlightConstantID = getUniform("spotlight.constant");
        spotlightAmbIntID = getUniform("spotlight.ambientIntensity");
        spotlightDiffIntID = getUniform("spotlight.diffuseIntensity");
        spotlightConcentrationID = getUniform("spotlight.concentration");
    }

    FloatBuffer plcColBuf = BufferUtils.createFloatBuffer(3);
    FloatBuffer plcPosBuf = BufferUtils.createFloatBuffer(3);
    FloatBuffer plcDirBuf = BufferUtils.createFloatBuffer(3);
    @Override
    public void setLight(SpotLightComponent plc) {
        glUniform1f(spotlightAmbIntID, plc.getAmbientIntensity());
        glUniform1f(spotlightDiffIntID, plc.getDiffuseIntensity());
        glUniform1f(spotlightLinearID, plc.getLinear());
        glUniform1f(spotlightExpID, plc.getExp());
        glUniform1f(spotlightConstantID, plc.getConstant());
        glUniform1f(spotlightConcentrationID, plc.getConcentration());

        plcColBuf.clear();
        plc.getColor().store(plcColBuf);
        plcColBuf.flip();
        glUniform3(spotlightColID, plcColBuf);

        plcPosBuf.clear();
        plc.getPosition().store(plcPosBuf);
        plcPosBuf.flip();
        glUniform3(spotlightPosID, plcPosBuf);

        plcDirBuf.clear();
        plc.getDir().store(plcDirBuf);
        plcDirBuf.flip();
        glUniform3(spotlightDirID, plcDirBuf);
    }
}

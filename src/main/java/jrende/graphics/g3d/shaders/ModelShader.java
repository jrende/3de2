package jrende.graphics.g3d.shaders;

import jrende.graphics.g3d.light.LightComponent;
import jrende.graphics.g3d.material.Material;
import org.lwjgl.util.vector.Vector3f;

import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL20.*;

public abstract class ModelShader<L extends LightComponent> extends Shader {

    private int specularIntensityID;
    private int gSamplerID;
    private int nSamplerID;
    private int sSamplerID;
    private int eyePosID;
    private int eyeDirID;
    private int specularExponentID;
    private int diffuseColorID;
    private int specularColorID;
    private int ambientColorID;

    public ModelShader(String shaderName) {
        super(shaderName);
        diffuseColorID = getUniform("ambient");
        specularColorID = getUniform("diffuse");
        ambientColorID = getUniform("specular");
        specularIntensityID = getUniform("gMatSpecularIntensity");
        specularExponentID = getUniform("gSpecularExponent");

        gSamplerID = getUniform("gSampler");
        nSamplerID = getUniform("nSampler");
        sSamplerID = getUniform("sSampler");

        eyePosID = getUniform("eyePos");
        eyeDirID = getUniform("eyeDir");
    }

    public void setGSampler(int gSampler) {
        glUniform1i(gSamplerID, gSampler);
    }

    public void setNSampler(int nSampler) {
        glUniform1i(nSamplerID, nSampler);
    }

    public void setSSampler(int sSampler) {
        glUniform1i(sSamplerID, sSampler);
    }

    public void setEyePos(FloatBuffer eyePos) {
        glUniform3(eyePosID, eyePos);
    }

    public void setEyeDir(FloatBuffer eyeDir) {
        glUniform3(eyeDirID, eyeDir);
    }

    public void setSpecularIntensity(float specularIntensity) {
        glUniform1f(specularIntensityID, specularIntensity);
    }

    public void setSpecularExponent(float specularExponent) {
        glUniform1f(specularExponentID, specularExponent);
    }

    public void setDiffuseColor(Vector3f color) {
        //glUniform3f(diffuseColorID, color.x, color.y, color.z);
    }

    public void setSpecularColor(Vector3f color) {
        //glUniform3f(specularColorID, color.x, color.y, color.z);
    }

    public void setAmbientColor(Vector3f color) {
        //glUniform3f(ambientColorID, color.x, color.y, color.z);
    }

    public void setMaterial(Material material) {
        setAmbientColor(material.ambient);
        setDiffuseColor(material.diffuse);
        setSpecularColor(material.specular);

        setSpecularIntensity(5.0f);
        setSpecularExponent(100f);
    }

    public abstract void setLight(L lightComponent);
}

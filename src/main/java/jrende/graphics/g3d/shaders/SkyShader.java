package jrende.graphics.g3d.shaders;

import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL20.*;

public class SkyShader extends Shader {

    public SkyShader() {
        super("SkyShader");
        shader.useShader();
    }

    public int getUniform(String name) {
        return shader.getUniform(name);
    }

    public void useShader() {
        shader.useShader();
    }
}

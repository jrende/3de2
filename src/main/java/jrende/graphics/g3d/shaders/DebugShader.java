package jrende.graphics.g3d.shaders;

import jrende.graphics.util.Vertex;
import jrende.graphics.util.VertexArray;
import jrende.util.LogUtil;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Vector3f;

import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL20.glUniform2f;
import static org.lwjgl.opengl.GL20.glUniform3f;
import static org.lwjgl.opengl.GL20.glUniformMatrix4;

public class DebugShader extends Shader {
    VertexArray points;

    public DebugShader() {
        super("Debug");

        GL11.glEnable(GL20.GL_VERTEX_PROGRAM_POINT_SIZE);
        shader.useShader();
        LogUtil.getGlError();

        points = new VertexArray(1, false, false, false, false);
        points.addVertex(new Vertex(new Vector3f(0, 0, 0)));
        /*
        points.addVertex(new Vertex(new Vector3f(1, 0, 0)));
        points.addVertex(new Vertex(new Vector3f(0, 1, 0)));
        points.addVertex(new Vertex(new Vector3f(1, 0, 0)));
        points.addVertex(new Vertex(new Vector3f(1, 1, 0)));
        points.addVertex(new Vertex(new Vector3f(0, 1, 0)));
        */
        points.flip();
    }

    public int getUniform(String name) {
        return shader.getUniform(name);
    }

    public void useShader() {
        shader.useShader();
    }
    public void drawPoint() {
        points.render(GL11.GL_POINTS);
    }

}

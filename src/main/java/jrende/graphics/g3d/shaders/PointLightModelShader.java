package jrende.graphics.g3d.shaders;

import jrende.graphics.g3d.light.PointLightComponent;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL20.*;

public class PointLightModelShader extends ModelShader<PointLightComponent> {
    private int pointLightColID;
    private int pointLightAmbIntID;
    private int pointLightDiffIntID;
    private int pointLightLinearID;
    private int pointLightExpID;
    private int pointLightConstantID;
    private int pointLightPosID;

    public PointLightModelShader() {
        super("PointLight");

        pointLightColID = getUniform("pointLight.color");
        pointLightPosID = getUniform("pointLight.position");
        pointLightExpID = getUniform("pointLight.exp");
        pointLightLinearID = getUniform("pointLight.linear");
        pointLightConstantID = getUniform("pointLight.constant");
        pointLightAmbIntID = getUniform("pointLight.ambientIntensity");
        pointLightDiffIntID = getUniform("pointLight.diffuseIntensity");
    }

    FloatBuffer plcColBuf = BufferUtils.createFloatBuffer(3);
    FloatBuffer plcPosBuf = BufferUtils.createFloatBuffer(3);
    @Override
    public void setLight(PointLightComponent plc) {
        glUniform1f(pointLightAmbIntID, plc.getAmbientIntensity());
        glUniform1f(pointLightDiffIntID, plc.getDiffuseIntensity());
        glUniform1f(pointLightLinearID, plc.getLinear());
        glUniform1f(pointLightExpID, plc.getExp());
        glUniform1f(pointLightConstantID, plc.getConstant());

        plcColBuf.clear();
        plc.getColor().store(plcColBuf);
        plcColBuf.flip();
        glUniform3(pointLightColID, plcColBuf);

        plcPosBuf.clear();
        plc.getPosition().store(plcPosBuf);
        plcPosBuf.flip();
        glUniform3(pointLightPosID, plcPosBuf);
    }

}

package jrende.graphics.g3d.shaders;

import java.util.HashMap;
import java.util.Map;

public class ShaderUtils {
    private static Map<String, ShaderProgram> shaderList = new HashMap<>();
    final static String shaderDir = "./shaders/";

    public static ShaderProgram getShader(String vert, String frag) {
        String key = vert + frag;
        ShaderProgram ret;
        if (shaderList.containsKey(key)) {
            ret = shaderList.get(key);
        } else {
            ret = new ShaderProgram(shaderDir + vert, shaderDir + frag);
            shaderList.put(vert + frag, ret);
        }
        return ret;
    }

}

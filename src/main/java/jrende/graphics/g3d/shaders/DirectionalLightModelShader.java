package jrende.graphics.g3d.shaders;

import jrende.graphics.g3d.light.DirectionalLightComponent;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL20.*;

public class DirectionalLightModelShader extends ModelShader<DirectionalLightComponent> {
    private int dirLightColID;
    private int dirLightAmbIntID;
    private int dirLightDirID;
    private int dirLightDiffIntID;

    public DirectionalLightModelShader() {
        super("DirLight");

        dirLightColID = getUniform("dirLight.color");
        dirLightAmbIntID = getUniform("dirLight.ambientIntensity");
        dirLightDirID = getUniform("dirLight.direction");
        dirLightDiffIntID = getUniform("dirLight.diffuseIntensity");
    }

    FloatBuffer colBuf = BufferUtils.createFloatBuffer(3);
    FloatBuffer dirBuf = BufferUtils.createFloatBuffer(3);
    @Override
    public void setLight(DirectionalLightComponent dlc) {
        glUniform1f(dirLightAmbIntID, dlc.getAmbientIntensity());
        glUniform1f(dirLightDiffIntID, dlc.getDiffuseIntensity());

        colBuf.clear();
        dlc.getColor().store(colBuf);
        colBuf.flip();
        glUniform3(dirLightColID, colBuf);

        dirBuf.clear();
        dlc.getDirection().store(dirBuf);
        dirBuf.flip();
        glUniform3(dirLightDirID, dirBuf);
    }

}

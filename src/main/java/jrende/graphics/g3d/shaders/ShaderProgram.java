package jrende.graphics.g3d.shaders;

import jrende.util.LogUtil;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.GL11;

import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.opengl.ARBShaderObjects.glGetUniformLocationARB;
import static org.lwjgl.opengl.GL20.*;


public class ShaderProgram {
    int vertexShader, fragmentShader, prog;
    String name;

    public ShaderProgram(String vsFilename, String fsFilename) {
        vertexShader = compile(GL_VERTEX_SHADER, vsFilename);
        fragmentShader = compile(GL_FRAGMENT_SHADER, fsFilename);
        prog = glCreateProgram();
        glAttachShader(prog, vertexShader);
        glAttachShader(prog, fragmentShader);
        glLinkProgram(prog);
        name = vsFilename.substring(vsFilename.lastIndexOf('/') + 1, vsFilename.lastIndexOf('.'));
    }

    private int compile(int type, String filename) {
        int shader = glCreateShader(type);
        if (shader == 0) {
            System.err.println(": Error creating shader.");
            return -1;
        }
        String shaderCode = "";
        String line;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filename));
            while ((line = reader.readLine()) != null) {
                shaderCode += line + "\n";
            }
        } catch (Exception e) {
            String typeString = "";
            switch (type) {
                case GL_FRAGMENT_SHADER:
                    typeString = "fragment shader";
                    break;
                case GL_VERTEX_SHADER:
                    typeString = "vertex shader";
                    break;
            }
            System.err.println("Failure reading code for " + typeString + ".");
            System.err.println("Filename: " + filename + ".");
            shader = 0;
        }
        glShaderSource(shader, shaderCode);
        glCompileShader(shader);
        if (ARBShaderObjects.glGetObjectParameteriARB(shader, ARBShaderObjects.GL_OBJECT_COMPILE_STATUS_ARB) == GL11.GL_FALSE) {
            printLogInfo(shader);
            shader = 0;
        }
        return shader;
    }

    public void useShader() {
        glUseProgram(prog);
    }

    public int getUniform(String name) {
        int loc = glGetUniformLocationARB(prog, name);
        if (loc < 0) {
            //System.err.println("Error: Can't find uniform " + name + " in shader " + this.name + ".");
        }
        return loc;
    }

    private static boolean printLogInfo(int obj) {
        IntBuffer iVal = BufferUtils.createIntBuffer(1);
        ARBShaderObjects.glGetObjectParameterARB(obj, ARBShaderObjects.GL_OBJECT_INFO_LOG_LENGTH_ARB, iVal);

        int length = iVal.get();
        if (length > 1) {
            // We have some info we need to output.
            ByteBuffer infoLog = BufferUtils.createByteBuffer(length);
            iVal.flip();
            ARBShaderObjects.glGetInfoLogARB(obj, iVal, infoLog);
            byte[] infoBytes = new byte[length];
            infoLog.get(infoBytes);
            String out = new String(infoBytes);
            System.out.println("Info log:\n" + out);
        } else
            return true;
        return false;
    }
}

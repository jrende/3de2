package jrende.graphics.g3d.shaders;

import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL20.glUniformMatrix4;

public class Shader {
    ShaderProgram shader;
    private int modelMatrixID;
    private int mvpMatrixID;

    public Shader(String shaderName) {
        this.shader = ShaderUtils.getShader(shaderName + ".vert", shaderName + ".frag" );
        useShader();
        mvpMatrixID = getUniform("mvpMatrix");
        modelMatrixID = getUniform("modelMatrix");
    }

    public int getUniform(String name) {
        return shader.getUniform(name);
    }

    public void useShader() {
        shader.useShader();
    }

    public void setModelMatrix(FloatBuffer modelMatrix) {
        glUniformMatrix4(modelMatrixID, false, modelMatrix);
    }

    public void setMVPMatrix(FloatBuffer mvpMatrix) {
        glUniformMatrix4(mvpMatrixID, false, mvpMatrix);
    }
}

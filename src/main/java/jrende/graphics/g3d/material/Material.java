package jrende.graphics.g3d.material;


import jrende.graphics.texture.Texture;
import jrende.graphics.texture.TextureType;
import org.lwjgl.util.vector.Vector3f;

import java.util.HashMap;
import java.util.Map;


public class Material {

    public String name;

    public Vector3f ambient;
    public Vector3f diffuse;
    public Vector3f specular;
    public float specularCoefficient;
    public float transparency;
    public float refractionIndex;
    public Map<TextureType, Texture> textures = new HashMap<>(3);



    //	map_Ka lenna.tga           # the ambient texture map
//	map_Kd lenna.tga           # the diffuse texture map (most of the time, it will be the same as the ambient texture map)
//	map_Ks lenna.tga           # specular color texture map
//	map_Ns lenna_spec.tga      # specular highlight component
//	map_d lenna_alpha.tga      # the alpha texture map
//	map_bump lenna_bump.tga    # some implementations use 'map_bump' instead of 'bump' below
//	bump lenna_bump.tga        # bump map (which by default uses luminance channel of the image)
//	disp lenna_disp.tga        # displacement map
//	decal lenna_stencil.tga    # stencil decal texture (defaults to 'matte' channel of the image)
}

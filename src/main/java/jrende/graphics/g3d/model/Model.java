package jrende.graphics.g3d.model;

import jrende.graphics.RenderObject;
import jrende.graphics.g3d.material.Material;
import jrende.graphics.texture.Texture2D;
import jrende.graphics.texture.TextureType;
import jrende.graphics.texture.Texture;
import jrende.graphics.util.ObjLoader;
import jrende.graphics.util.VertexArray;
import jrende.util.LogUtil;

import java.nio.FloatBuffer;
import java.util.HashMap;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;


public class Model implements RenderObject {
    private VertexArray vertices;
    private Material material;
    private HashMap<TextureType, Integer> typeToTextureUnit = new HashMap<>();
    private String name;

    public Model() {}
    public Model(String filename) {
        initBuffer(filename);
    }

    private void initBuffer(String filename) {
        ObjLoader objLoader = new ObjLoader(filename);
        vertices = objLoader.getVertexArray();
        material = objLoader.getMaterial();
        setupTextures();
    }

    private void setupTextures() {
        Texture diffuseMap = material.textures.get(TextureType.DiffuseMap);
        if(diffuseMap == null) {
            material.textures.put(TextureType.DiffuseMap, new Texture2D(0.5f, 0.5f, 1.0f));
        }
        Texture specularMap = material.textures.get(TextureType.SpecularHighlightMap);
        if(specularMap == null) {
            material.textures.put(TextureType.SpecularHighlightMap, new Texture2D(0, 0, 0));
        }
        Texture normalMap = material.textures.get(TextureType.BumpMap);
        if(normalMap == null) {
            material.textures.put(TextureType.BumpMap, new Texture2D(0.5f, 0.5f, 1.0f));
        }
        typeToTextureUnit.put(TextureType.DiffuseMap, GL_TEXTURE0);
        typeToTextureUnit.put(TextureType.BumpMap, GL_TEXTURE1);
        typeToTextureUnit.put(TextureType.SpecularHighlightMap, GL_TEXTURE2);
    }

    @Override
    public void render() {
        vertices.render();
    }

    public void bindTexture(TextureType type) {
        glActiveTexture(type.getTextureUnit());
        glBindTexture(GL_TEXTURE_2D, material.textures.get(type).getTexID());
    }

    @Override
    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
        setupTextures();
    }

    public void setVertices(VertexArray vertices) {
        this.vertices = vertices;
    }

    public VertexArray getVertices() {
        return vertices;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

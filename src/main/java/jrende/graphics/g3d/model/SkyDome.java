package jrende.graphics.g3d.model;

import jrende.graphics.Camera;
import jrende.graphics.g3d.shaders.SkyShader;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL11.*;

public class SkyDome {
    Model dome;
    SkyShader shader;
    Matrix4f modelMatrix = new Matrix4f();
    Camera camera;
    FloatBuffer modelMatrixBuffer = BufferUtils.createFloatBuffer(4*4);
    public SkyDome() {
        dome = new Model("SkyDome16.obj");
        shader = new SkyShader();
    }

    public void render(FloatBuffer mvpMatrix) {
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_EQUAL);
        glDepthRange(1., 1.);

        shader.useShader();
        shader.setMVPMatrix(mvpMatrix);

        modelMatrix.setIdentity();
        modelMatrix.translate(camera.getPos());

        modelMatrixBuffer.clear();
        modelMatrix.store(modelMatrixBuffer);
        modelMatrixBuffer.flip();

        shader.setModelMatrix(modelMatrixBuffer);
        dome.render();
        glDepthFunc(GL11.GL_LEQUAL);
    }


    public void setCamera(Camera camera) {
        this.camera = camera;
    }
}

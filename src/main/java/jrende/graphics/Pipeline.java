package jrende.graphics;

import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import java.nio.FloatBuffer;
import java.util.LinkedList;
import java.util.List;

import static jrende.graphics.util.MathUtils.*;


public class Pipeline {
    Vector3f scale;
    Vector3f worldPos;
    Vector3f rotate;

    float fov;
    float width;
    float height;
    float zNear;
    float zFar;

    Vector3f camPos;
    Vector3f camTarget;
    Vector3f camUp;

    Matrix4f modelMatrix;
    Matrix4f viewMatrix;
    Matrix4f projMatrix;

    List<Matrix4f> matList = new LinkedList<>();


    public Pipeline() {
        scale = new Vector3f(1.0f, 1.0f, 1.0f);
        worldPos = new Vector3f(0.0f, 0.0f, 0.0f);
        rotate = new Vector3f(0.0f, 0.0f, 0.0f);

        viewMatrix = new Matrix4f();
        projMatrix = new Matrix4f();
        modelMatrix = new Matrix4f();
    }

    public void transform(Matrix4f modelMatrix) {
        Matrix4f.mul(modelMatrix, this.modelMatrix, this.modelMatrix);
    }

    FloatBuffer modelBuf = BufferUtils.createFloatBuffer(4 * 4);

    public FloatBuffer getModelMatrixBuf() {
        modelMatrix.store(modelBuf);
        modelBuf.flip();
        return modelBuf;
    }

    public void setPerspective(float fov, float width, float height, float zNear, float zFar) {
        this.fov = fov;
        this.width = width;
        this.height = height;
        this.zNear = zNear;
        this.zFar = zFar;
    }

    public void setCamera(Vector3f pos, Vector3f target, Vector3f up) {
        this.camPos = pos;
        this.camTarget = target;
        this.camUp = up;
    }

    Matrix4f camRotateTrans = new Matrix4f();
    Matrix4f camTranslateTrans = new Matrix4f();
    Matrix4f perspectiveTrans = new Matrix4f();

    public void calcVPMatrix() {
        camRotateTrans.setIdentity();
        camTranslateTrans.setIdentity();
        perspectiveTrans.setIdentity();
        initTranslationTransform(camTranslateTrans, -camPos.x, -camPos.y, -camPos.z);
        initCameraTransform(camRotateTrans, camTarget, camUp);
        initPersProj(perspectiveTrans, fov, width, height, zNear, zFar);

        viewMatrix.setIdentity();
        projMatrix.setIdentity();
        Matrix4f.mul(camTranslateTrans, viewMatrix, viewMatrix);
        Matrix4f.mul(camRotateTrans, viewMatrix, viewMatrix);
        Matrix4f.mul(perspectiveTrans, projMatrix, projMatrix);
    }

    public Matrix4f getViewMatrix() {
        return viewMatrix;
    }

    public Matrix4f getProjectionMatrix() {
        return projMatrix;
    }

    int level = 0;

    public void pushMatrix() {
        //Reuse matrices to avoid GC
        if (matList.size() > (level + 1)) {
            matList.get(level + 1).load(modelMatrix);
        } else {
            matList.add(new Matrix4f(modelMatrix));
        }
        level++;
    }

    public Matrix4f popMatrix() {
        modelMatrix = matList.remove(matList.size() - 1);
        level--;
        return modelMatrix;
    }

}

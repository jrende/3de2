package jrende.graphics;

import com.artemis.Component;
import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import java.nio.FloatBuffer;


public class TransformComponent extends Component {
    public Matrix4f modelMatrix = new Matrix4f();
    public Matrix4f setMatrix = new Matrix4f();

    public Vector3f scale = new Vector3f(1.0f, 1.0f, 1.0f);
    public Vector3f pos = new Vector3f();
    public Quaternion rot = new Quaternion();
    FloatBuffer t;

    public TransformComponent() {
        modelMatrix = new Matrix4f();
        t = BufferUtils.createFloatBuffer(4 * 4);
        t.put(1.0f).put(0.0f).put(0.0f).put(0.0f);
        t.put(0.0f).put(1.0f).put(0.0f).put(0.0f);
        t.put(0.0f).put(0.0f).put(1.0f).put(0.0f);
        t.put(0.0f).put(0.0f).put(0.0f).put(1.0f);
        t.flip();
    }

    public Matrix4f getModelMatrix() {
        modelMatrix.setIdentity();
        modelMatrix.translate(pos);
        modelMatrix.scale(scale);
        Matrix4f.mul(modelMatrix, setMatrix, modelMatrix);
        return modelMatrix;
    }

    public void transform(float[] mat) {
        if (mat.length != 4 * 4) {
            return;
        }
        t.clear();
        t.put(mat);
        t.flip();
        setMatrix.load(t);
    }

    public void rotate(Vector4f rotation) {
        rot.setFromAxisAngle(rotation);
    }

    public void scale(Vector3f scale) {
        this.scale.x *= scale.x;
        this.scale.y *= scale.y;
        this.scale.z *= scale.z;
    }

    public void translate(Vector3f pos) {
        this.pos.translate(pos.x, pos.y, pos.z);
    }

    public Vector3f getPos() {
        return pos;
    }

    public Vector3f getScale() {
        return scale;
    }
}

package jrende.graphics;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.EntitySystem;
import com.artemis.annotations.Mapper;
import com.artemis.utils.ImmutableBag;
import jrende.graphics.g3d.light.DirectionalLightComponent;
import jrende.graphics.g3d.light.LightComponent;
import jrende.graphics.g3d.light.PointLightComponent;
import jrende.graphics.g3d.light.SpotLightComponent;
import jrende.graphics.g3d.material.Material;
import jrende.graphics.g3d.model.SkyDome;
import jrende.graphics.g3d.shaders.*;
import jrende.graphics.texture.TextureType;
import jrende.game.Settings;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.Util;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL43.glMultiDrawElementsIndirect;

public class Renderer extends EntitySystem {
    Camera camera;
    public float fov = 45;

    Map<Class, ModelShader> shaders = new HashMap<>();
    List<RenderObject> renderList = new ArrayList<>();
    List<LightComponent> lights = new ArrayList<>();
    List<Entity> visibleEntities = new ArrayList<>();
    SkyDome sky = new SkyDome();
    DebugRenderer debugRenderer = new DebugRenderer();

    @Mapper
    ComponentMapper<TransformComponent> transformMapper;
    @Mapper
    ComponentMapper<RenderComponent> renderMapper;

    public Renderer() {
        super(Aspect.getAspectForAll(TransformComponent.class, RenderComponent.class));
        shaders.put(DirectionalLightComponent.class, new DirectionalLightModelShader());
        shaders.put(PointLightComponent.class, new PointLightModelShader());
        shaders.put(SpotLightComponent.class, new SpotLightModelShader());
        initGL();
    }

    public void initGL() {
        //glClearColor(0.139f, 0.223f, 0.469f, 1.0f);
        glClearColor(0, 0, 0, 1);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glDepthMask(true);

        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        glFrontFace(GL_CCW);
    }

    Pipeline pipeline = new Pipeline();
    @Override
    protected void begin() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glDisable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE);
        glDepthRange(0.0, 1.0);
        camera.onRender();

        pipeline.setCamera(camera.getPos(), camera.getTarget(), camera.getUp());
        pipeline.setPerspective(fov, Display.getWidth(), Display.getHeight(), 1f, 1000f);
        pipeline.calcVPMatrix();

        Matrix4f viewMatrix = pipeline.getViewMatrix();
        Matrix4f projectionMatrix = pipeline.getProjectionMatrix();
        Matrix4f.mul(projectionMatrix, viewMatrix, vpMat);
        vpMat.store(vpBuf);
        vpBuf.flip();

    }

    private FloatBuffer eyePos = BufferUtils.createFloatBuffer(3);
    private FloatBuffer eyeDir = BufferUtils.createFloatBuffer(3);
    private FloatBuffer vpBuf = BufferUtils.createFloatBuffer(4 * 4);
    private FloatBuffer mvpBuf = BufferUtils.createFloatBuffer(4 * 4);
    private FloatBuffer modelMatrixBuffer = BufferUtils.createFloatBuffer(4 * 4);
    private Matrix4f vpMat = new Matrix4f();
    private Matrix4f mvpMat = new Matrix4f();

    boolean test = false;
    Vector3f[] dots;
    @Override
    protected void processEntities(ImmutableBag<Entity> entities) {
        List<Entity> visibleEntities = getVisibleEntities(entities);
        int lightIndex = -1;
        for(Vector3f p: dots) {
            debugRenderer.drawPoint(p, vpBuf);
        }
        for (LightComponent light : lights) {
            if(Settings.getDebug()) {
                debugRenderer.drawPoint(light.getPosition(), vpBuf);
            }

            lightIndex++;
            if(currentLight != -1 && currentLight != lightIndex) {
               continue;
            }
            ModelShader shader = shaders.get(light.getClass());
            shader.useShader();
            for (int i = 0; i < entities.size(); i++) {
                Entity e = entities.get(i);
                RenderComponent renderComponent = renderMapper.get(e);
                TransformComponent transformComponent = transformMapper.get(e);
                Material material = renderComponent.getMaterial();
                shader.setMaterial(material);

                modelMatrixBuffer.clear();
                transformComponent.getModelMatrix().store(modelMatrixBuffer);
                modelMatrixBuffer.flip();
                shader.setModelMatrix(modelMatrixBuffer);

                Matrix4f.mul(vpMat, transformComponent.getModelMatrix(), mvpMat);
                mvpBuf.clear();
                mvpMat.store(mvpBuf);
                mvpBuf.flip();
                shader.setMVPMatrix(mvpBuf);

                shader.setLight(light);

                eyeDir.clear();
                camera.target.store(eyeDir);
                eyeDir.flip();
                shader.setEyeDir(eyeDir);

                eyePos.clear();
                camera.pos.store(eyePos);
                eyePos.flip();
                shader.setEyePos(eyePos);

                shader.setGSampler(0);
                renderComponent.bindTexture(TextureType.DiffuseMap);
                shader.setNSampler(1);
                renderComponent.bindTexture(TextureType.BumpMap);
                shader.setSSampler(2);
                renderComponent.bindTexture(TextureType.SpecularHighlightMap);
                renderComponent.render();
            }
            //Enable blending for the rest of the lights
            glEnable(GL_BLEND);
        }
    }

    @Override
    protected void end() {
        sky.render(vpBuf);
        Util.checkGLError();
        Display.update();
    }

    private List<Entity> getVisibleEntities(ImmutableBag<Entity> entities) {
        visibleEntities.clear();
        //pipeline.getViewFrustum();
        return visibleEntities;
    }

    @Override
    protected boolean checkProcessing() {
        return true;
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
        sky.setCamera(camera);
    }

    public void add(RenderObject e) {
        renderList.add(e);
    }

    public void enableBackfaceCulling(boolean enable) {
        if (enable) {
            glEnable(GL_CULL_FACE);
        } else {
            glDisable(GL_CULL_FACE);
        }
    }

    public void setCullFace(int mode) {
        glCullFace(mode);
    }

    public void enableWireframe(boolean enable) {
        glPolygonMode(GL_FRONT_AND_BACK, enable ? GL_LINE : GL_FILL);
    }

    public void setFOV(float fov) {
        this.fov = fov;
    }

    public float getFOV() {
        return fov;
    }

    public Camera getActiveCamera() {
        return camera;
    }

    public void addLight(LightComponent plc) {
        //if(plc instanceof DirectionalLightComponent)
        lights.add(plc);
    }

    int currentLight = -1;

    public void drawOnlyLight(int i) {
        System.out.println("Draw only light " + (i + 1) + " out of " + lights.size());
        currentLight = i;
    }

    public List<LightComponent> getLights() {
        return lights;
    }

    public LightComponent getOnlyLight(int i) {
        if(i < lights.size()) {
            return lights.get(i);
        } else {
            return null;
        }
    }

    public void drawAllLights() {
        System.out.println("Draw all lights");
        currentLight = -1;
    }

    public LightComponent getCurrentLight() {
       return lights.get(currentLight);
    }
}

package jrende.graphics;


import jrende.graphics.g3d.material.Material;
import jrende.graphics.texture.TextureType;

import java.nio.FloatBuffer;

import java.nio.FloatBuffer;


public interface RenderObject {
    public void render();
    public Material getMaterial();
    public void bindTexture(TextureType type);
}

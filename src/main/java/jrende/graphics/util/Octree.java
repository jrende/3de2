package jrende.graphics.util;

import org.lwjgl.util.vector.Vector3f;

public class Octree {
    OctNode root;

    public Octree(Vector3f origin, int halfSize) {
        root = new OctNode(origin, halfSize);
    }

    public void insert(AABB point) {
        root.insert(point);
    }

    public int size() {
        return root.numPoints()/8;
    }
}

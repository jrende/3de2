package jrende.graphics.util;

import java.util.List;

public class VertexArrayBuilder {
    public VertexArray buildVertexArray(List<Face> faces) {
        VertexArray vArray = new VertexArray(3*faces.size(), false);
        for(Face f: faces) {
            for(Vertex v: f.asArray()) {
                vArray.addVertex(v);
            }
        }
        vArray.flip();
        return vArray;
    }
}

package jrende.graphics.util;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

public class MathUtils {
    public static void rotate(float angle, Vector3f axis, Vector3f dest) {
//		System.out.println("pre rot: " + dest.toString());
        float sinHalf = (float) Math.sin(ToRadian(angle / 2));
        float cosHalf = (float) Math.cos(ToRadian(angle / 2));

        float rx = axis.x * sinHalf;
        float ry = axis.y * sinHalf;
        float rz = axis.z * sinHalf;
        float rw = cosHalf;
        Quaternion rotationQ = new Quaternion(rx, ry, rz, rw);
        Quaternion conjugateQ = new Quaternion(-rx, -ry, -rz, rw);

        Quaternion w = new Quaternion();
        //Multiplicera en quaternion och en vektor
        Quaternion.mul(w, conjugateQ, w);
        multiply(dest, rotationQ, w);
        //W = rotQ * this * conjQ
        dest.x = w.x;
        dest.y = w.y;
        dest.z = w.z;
//		System.out.println("post rot: " + dest.toString())
    }

    public static void initScaleTransform(Matrix4f m, float x, float y, float z) {
        m.m00 = x;
        m.m01 = 0.0f;
        m.m02 = 0.0f;
        m.m03 = 0.0f;
        m.m10 = 0.0f;
        m.m11 = y;
        m.m12 = 0.0f;
        m.m13 = 0.0f;
        m.m20 = 0.0f;
        m.m21 = 0.0f;
        m.m22 = z;
        m.m23 = 0.0f;
        m.m30 = 0.0f;
        m.m31 = 0.0f;
        m.m32 = 0.0f;
        m.m33 = 1.0f;

    }

    public static void initRotateTransform(Matrix4f m, float rotX, float rotY, float rotZ) {
        Matrix4f rx = new Matrix4f();
        Matrix4f ry = new Matrix4f();
        Matrix4f rz = new Matrix4f();

        float x = ToRadian(rotX);
        float y = ToRadian(rotY);
        float z = ToRadian(rotZ);

        rx.m00 = 1.0f;
        rx.m01 = 0.0f;
        rx.m02 = 0.0f;
        rx.m03 = 0.0f;
        rx.m10 = 0.0f;
        rx.m11 = (float) Math.cos(x);
        rx.m12 = (float) -Math.sin(x);
        rx.m13 = 0.0f;
        rx.m20 = 0.0f;
        rx.m21 = (float) Math.sin(x);
        rx.m22 = (float) Math.cos(x);
        rx.m23 = 0.0f;
        rx.m30 = 0.0f;
        rx.m31 = 0.0f;
        rx.m32 = 0.0f;
        rx.m33 = 1.0f;

        ry.m00 = (float) Math.cos(y);
        ry.m01 = 0.0f;
        ry.m02 = (float) -Math.sin(y);
        ry.m03 = 0.0f;
        ry.m10 = 0.0f;
        ry.m11 = 1.0f;
        ry.m12 = 0.0f;
        ry.m13 = 0.0f;
        ry.m20 = (float) Math.sin(y);
        ry.m21 = 0.0f;
        ry.m22 = (float) Math.cos(y);
        ry.m23 = 0.0f;
        ry.m30 = 0.0f;
        ry.m31 = 0.0f;
        ry.m32 = 0.0f;
        ry.m33 = 1.0f;

        rz.m00 = (float) Math.cos(z);
        rz.m01 = (float) -Math.sin(z);
        rz.m02 = 0.0f;
        rz.m03 = 0.0f;
        rz.m10 = (float) Math.sin(z);
        rz.m11 = (float) Math.cos(z);
        rz.m12 = 0.0f;
        rz.m13 = 0.0f;
        rz.m20 = 0.0f;
        rz.m21 = 0.0f;
        rz.m22 = 1.0f;
        rz.m23 = 0.0f;
        rz.m30 = 0.0f;
        rz.m31 = 0.0f;
        rz.m32 = 0.0f;
        rz.m33 = 1.0f;

        Matrix4f.mul(m, rz, m);
        Matrix4f.mul(m, ry, m);
        Matrix4f.mul(m, rx, m);
    }

    static Vector3f trVec = new Vector3f();

    public static void initTranslationTransform(Matrix4f m, float x, float y, float z) {
        trVec.set(x, y, z);
        Matrix4f.translate(trVec, m, m);
//		m.m00 = 1.0f; m.m01 = 0.0f; m.m02 = 0.0f; m.m03 = x;
//		m.m10 = 0.0f; m.m11 = 1.0f; m.m12 = 0.0f; m.m13 = y;
//		m.m20 = 0.0f; m.m21 = 0.0f; m.m22 = 1.0f; m.m23 = z;
//		m.m30 = 0.0f; m.m31 = 0.0f; m.m32 = 0.0f; m.m33 = 1.0f;
    }

    static Vector3f N = new Vector3f();
    static Vector3f U = new Vector3f();
    static Vector3f V = new Vector3f();

    public static void initCameraTransform(Matrix4f m, Vector3f target, Vector3f up) {
        N.set(target);
        N.normalise();
        U.set(up);
        U.normalise();
        Vector3f.cross(U, N, U);
        Vector3f.cross(N, U, V);

        m.m00 = U.x;
        m.m01 = U.y;
        m.m02 = U.z;
        m.m03 = 0.0f;
        m.m10 = V.x;
        m.m11 = V.y;
        m.m12 = V.z;
        m.m13 = 0.0f;
        m.m20 = N.x;
        m.m21 = N.y;
        m.m22 = N.z;
        m.m23 = 0.0f;
        m.m30 = 0.0f;
        m.m31 = 0.0f;
        m.m32 = 0.0f;
        m.m33 = 1.0f;
    }

    public static void initPersProj(Matrix4f m, float FOV, float Width, float Height, float zNear, float zFar) {
//		float ar = ((float) Width/Height);
//		float zRange = zNear - zFar;
//		float tanHalfFov = (float) Math.tan(ToRadian(FOV / 2.0f));
//		
//		m.m00 = 1.0f/(tanHalfFov * ar);	m.m01 = 0.0f;			m.m02 = 0.0f;					m.m03 = 0.0f;
//		m.m10 = 0.0f;					m.m11 = 1.0f/tanHalfFov;m.m12 = 0.0f;					m.m13 = 0.0f;
//		m.m20 = 0.0f;					m.m21 = 0.0f;			m.m22 = (-zNear - zFar)/zRange;	m.m23 = -1.0f;
//		m.m30 = 0.0f;					m.m31 = 0.0f;			m.m32 = 1.0f;					m.m33 = 0.0f;

        float ar = ((float) Width) / ((float) Height);
        float f = (float) (1 / Math.tan(ToRadian(FOV / 2)));

        m.m00 = f / ar;
        m.m01 = 0;
        m.m02 = 0;
        m.m03 = 0;
        m.m10 = 0;
        m.m11 = f;
        m.m12 = 0;
        m.m13 = 0;
        m.m20 = 0;
        m.m21 = 0;
        m.m22 = (zFar + zNear) / (zNear - zFar);
        m.m23 = (2 * zFar * zNear) / (zNear - zFar);
        m.m30 = 0;
        m.m31 = 0;
        m.m32 = -1;
        m.m33 = 0;
    }

    public static void initOrthoProj(Matrix4f m, float left, float right, float bottom, float top, float near, float far) {
        float tx = -((right + left) / (right - left));
        float ty = -((top + bottom) / (top - bottom));
        float tz = -((far + near) / (far - near));
        m.m00 = 2.0f / (right - left);
        m.m01 = 0.0f;
        m.m02 = 0.0f;
        m.m03 = tx;
        m.m10 = 0.0f;
        m.m11 = 2.0f / (top - bottom);
        m.m12 = 0.0f;
        m.m13 = ty;
        m.m20 = 0.0f;
        m.m21 = 0.0f;
        m.m22 = (-2.0f) / (far - near);
        m.m23 = tz;
        m.m30 = 0.0f;
        m.m31 = 0.0f;
        m.m32 = 0.0f;
        m.m33 = 1.0f;
//		System.out.println(m.toString());
    }

    public static float ToRadian(float deg) {
        return (float) (deg * (Math.PI / 180));
    }

    public static float ToDegree(double rad) {
        return (float) (rad * (180 / Math.PI));
    }

    public static void multiply(Vector3f v, Quaternion q, Quaternion dest) {
//		float w = - (q.x * v.x) - (q.y * v.y) - (q.z * v.z);
//		float x =   (q.w * v.x) + (q.y * v.z) - (q.z * v.y);
//		float y =   (q.w * v.y) + (q.z * v.x) - (q.x * v.z);
//		float z =   (q.w * v.z) + (q.x * v.y) - (q.y * v.x);

        float w = -(q.x * v.x) - (q.y * v.y) - (q.z * v.z);
        float x = (q.w * v.x) + (q.y * v.z) - (q.z * v.y);
        float y = (q.w * v.y) + (q.z * v.x) - (q.x * v.z);
        float z = (q.w * v.z) + (q.x * v.y) - (q.y * v.x);

        dest.x = x;
        dest.y = y;
        dest.z = z;
        dest.w = w;
    }

    public static void multiply(Quaternion l, Quaternion r, Quaternion dest) {
        float w = (l.w * r.w) - (l.x * r.x) - (l.y * r.y) - (l.z * r.z);
        float x = (l.x * r.w) + (l.w * r.x) + (l.y * r.z) - (l.z * r.y);
        float y = (l.y * r.w) + (l.w * r.y) + (l.z * r.x) - (l.x * r.z);
        float z = (l.z * r.w) + (l.w * r.z) + (l.x * r.y) - (l.y * r.x);

        dest.x = x;
        dest.y = y;
        dest.z = z;
        dest.w = w;
    }

    public static void multiply(Vector4f v, Matrix4f m, Vector4f dest) {
//		float w = 1;	//?
        dest.x = (m.m02 * v.z + m.m01 * v.y + m.m00 * v.x + m.m03 * v.w);
        dest.y = (m.m12 * v.z + m.m11 * v.y + m.m10 * v.x + m.m13 * v.w);
        dest.z = (m.m22 * v.z + m.m21 * v.y + m.m20 * v.x + m.m23 * v.w);
        dest.w = (m.m32 * v.z + m.m31 * v.y + m.m30 * v.x + m.m33 * v.w);
    }

    public static Vector3f normal(Vector3f p1, Vector3f p2, Vector3f p3, Vector3f dest) {
        Vector3f U = Vector3f.sub(p2, p1, null);
        Vector3f V = Vector3f.sub(p3, p1, null);

        if (dest == null) {
            dest = new Vector3f();
        }
        dest.x = (U.y * V.z) - (U.z * V.y);
        dest.y = (U.z * V.z) - (U.x * V.y);
        dest.z = (U.x * V.y) - (U.y * V.x);
        if (dest.length() < 0) {
            dest.normalise();
        }
        return dest;
    }

    public static Vector4f quatToAxisAngle(Quaternion q, Vector4f out) {
        if (out == null) {
            out = new Vector4f();
        }
        /*
            angle = 2 * acos(qw)
            x = qx / sqrt(1-qw*qw)
            y = qy / sqrt(1-qw*qw)
            z = qz / sqrt(1-qw*qw)
         */
        float f = (float) Math.sqrt(1 - q.w * q.w);
        out.x = q.x / f;
        out.y = q.y / f;
        out.z = q.z / f;
        out.w = 2 * (float) Math.acos(q.w);
        return out;
    }
}

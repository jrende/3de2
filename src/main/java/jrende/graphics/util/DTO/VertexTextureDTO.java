package jrende.graphics.util.DTO;


import jrende.graphics.texture.Texture;
import jrende.graphics.util.VertexArray;

public class VertexTextureDTO {
    public VertexArray vertexArray;
    public Texture texture;
}
package jrende.graphics.util;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_INT;
import static org.lwjgl.opengl.GL11.glDrawElements;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;
public class VertexArray {
    FloatBuffer vertexBuffer;
	int posLength = 3;
	int texLength = 2;
	int norLength = 3;
    int tanLength = 3;

	int indexBufId;
	int vaoID;

    public final int vertexSize;
    boolean isDynamic;
    private boolean hasTex;
    private final boolean hasNor;
    private final boolean hasTan;

	public VertexArray(int vertexSize, boolean isDynamic) {
        this(vertexSize, isDynamic, true, true, true);
	}
    public VertexArray(int vertexSize, boolean isDynamic, boolean hasTex, boolean hasNor, boolean hasTan) {
        this.vertexSize = vertexSize;
        this.isDynamic = isDynamic;
        this.hasTex = hasTex;
        this.hasNor = hasNor;
        this.hasTan = hasTan;
        if(!hasTex) texLength = 0;
        if(!hasNor) norLength = 0;
        if(!hasTan) tanLength = 0;

        vertexBuffer = BufferUtils.createFloatBuffer((posLength+texLength+norLength+2*tanLength) * vertexSize);
    }

    public void addVertex(Vertex v) {
        v.getPos().store(vertexBuffer);
        if(hasTex) v.getTex().store(vertexBuffer);
        if(hasNor) v.getNormal().store(vertexBuffer);
        if(hasTan){
            v.getTangent().store(vertexBuffer);
            v.getBitangent().store(vertexBuffer);
        }
    }

	public void flip() {
        vaoID = glGenVertexArrays();
		glBindVertexArray(vaoID);
        vertexBuffer.flip();

        int stride = 4*(posLength+texLength+norLength+2*tanLength);

        int vboId = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, vboId);
        glBufferData(GL_ARRAY_BUFFER, vertexBuffer, isDynamic ? GL_STATIC_DRAW : GL_DYNAMIC_DRAW);

        enableVertexAttribArrays();
        glVertexAttribPointer(0, posLength, GL_FLOAT, false, stride, 0);
        if(hasTex) glVertexAttribPointer(1, texLength, GL_FLOAT, false, stride, 4*(posLength));
        if(hasNor) glVertexAttribPointer(2, norLength, GL_FLOAT, false, stride, 4*(posLength+texLength));
        if(hasTan) {
            glVertexAttribPointer(3, tanLength, GL_FLOAT, false, stride, 4*(posLength+texLength+norLength));
            glVertexAttribPointer(4, tanLength, GL_FLOAT, false, stride, 4*(posLength+texLength+norLength+tanLength));
        }
        disableVertexAttribArrays();

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);

		IntBuffer indexBuf = BufferUtils.createIntBuffer(vertexSize);
		for (int i = 0; i < vertexSize; i++) {
			indexBuf.put(i);
		}
		indexBuf.flip();
		indexBufId = glGenBuffers();
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufId);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexBuf, isDynamic ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

    public void render() {
       render(GL_TRIANGLES);
    }

	public void render(int primitive) {
		glBindVertexArray(vaoID);

        enableVertexAttribArrays();
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufId);
		glDrawElements(primitive, vertexSize, GL_UNSIGNED_INT, 0);
        disableVertexAttribArrays();

		glBindVertexArray(0);
	}

    private void enableVertexAttribArrays() {
        glEnableVertexAttribArray(0);
        if(hasTex) glEnableVertexAttribArray(1);
        if(hasNor) glEnableVertexAttribArray(2);
        if(hasTan) {
            glEnableVertexAttribArray(3);
            glEnableVertexAttribArray(4);
        }
    }

    private void disableVertexAttribArrays() {
        glDisableVertexAttribArray(0);
        if(hasTex) glDisableVertexAttribArray(1);
        if(hasNor) glDisableVertexAttribArray(2);
        if(hasTan) {
            glDisableVertexAttribArray(3);
            glDisableVertexAttribArray(4);
        }
    }
}

package jrende.graphics.util;

import org.lwjgl.util.vector.Vector3f;

public class AABB {
    Vector3f[] points;

    private int ret(int i, int j) {
       return  (i&j) == 0 ? 1 : 0;
    }
    public AABB(Vector3f origin, int halfSizeX, int halfSizeY, int halfSizeZ) {
        points = new Vector3f[8];
        for (int i = 0; i < 8; i++) {
            points[i] = new Vector3f(origin);
            points[i].x = halfSizeZ * ((i&1)==0 ? .5f : -.5f);
            points[i].y = halfSizeY * ((i&2)==0 ? .5f : -.5f);
            points[i].z = halfSizeX * ((i&4)==0 ? .5f : -.5f);
        }
    }
}

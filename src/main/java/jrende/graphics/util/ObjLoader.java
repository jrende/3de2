package jrende.graphics.util;


import jrende.graphics.g3d.material.Material;
import jrende.graphics.g3d.model.Model;
import jrende.graphics.texture.Texture2D;
import jrende.graphics.texture.TextureType;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


public class ObjLoader {
    private final String modelDir = "gfx/";
    private final VertexArrayBuilder vertexArrayBuilder;
    private TextureBuilder textureBuilder;
    List<Vector3f> positionList = new ArrayList<>();
    List<Vector2f> uvCoordinateList = new ArrayList<>();
    List<Vector3f> normalList = new ArrayList<>();
    Map<String, Material> materials = new HashMap<>();
    List<Model> modelList = new ArrayList<>();
    List<Face> faces = new ArrayList<>();

    public ObjLoader(String filename) {
        this(filename, new VertexArrayBuilder(), new TextureBuilder());
    }

    public ObjLoader(String filename, VertexArrayBuilder vertexArrayBuilder, TextureBuilder textureBuilder) {
        this.vertexArrayBuilder = vertexArrayBuilder;
        this.textureBuilder = textureBuilder;
        loadModel(filename);
    }

    public List<Model> loadModel(String filename) {
        loadFile(modelDir + filename);
        Model currentModel = null;
        String lastMaterialName = null;
        String lastName = null;
        for(String line: loadFile(modelDir + filename)) {
            StringTokenizer st = new StringTokenizer(line.trim());
            if(!line.isEmpty())
            switch (st.nextToken()) {
                case "v":
                    positionList.add(new Vector3f(Float.parseFloat(st.nextToken()), Float.parseFloat(st.nextToken()), Float.parseFloat(st.nextToken())));
                    break;
                case "vt":
                    uvCoordinateList.add(new Vector2f(Float.parseFloat(st.nextToken()), Float.parseFloat(st.nextToken())));
                    break;
                case "vn":
                    normalList.add(new Vector3f(Float.parseFloat(st.nextToken()), Float.parseFloat(st.nextToken()), Float.parseFloat(st.nextToken())));
                    break;
                case "mtllib":
                    loadMaterials(modelDir + st.nextToken());
                    //mtllib = st.nextToken();
                    break;
                case "g": case "o":
                    if(currentModel != null) {
                        currentModel.setVertices(vertexArrayBuilder.buildVertexArray(faces));
                        faces.clear();
                        modelList.add(currentModel);
                    }
                    currentModel = new Model();
                    lastName = st.nextToken();
                    currentModel.setName(lastName);
                    break;
                case "usemtl":
                    lastMaterialName = st.nextToken();
                    currentModel.setMaterial(materials.get(lastMaterialName));
                    break;
                case "f":
                    String[] tokens = line.substring(2).split(" ");
                    switch (tokens.length) {
                        case 4: {
                            Vertex[] verts = new Vertex[4];
                            for (int i = 0; i < 4; i++) {
                                String[] parts = tokens[i].split("/");
                                verts[i] = setTangents(new Vertex(
                                        positionList.get(Integer.valueOf(parts[0]) - 1),
                                        uvCoordinateList.get(Integer.valueOf(parts[1]) - 1),
                                        normalList.get(Integer.valueOf(parts[2]) - 1)));
                            }
                            faces.add(new Face(verts[0], verts[2], verts[3]));
                        }
                        //sic
                        case 3: {
                            Vertex[] verts = new Vertex[3];
                            for (int i = 0; i < 3; i++) {
                                String[] parts = tokens[i].split("/");
                                verts[i] = setTangents(new Vertex(
                                        positionList.get(Integer.valueOf(parts[0]) - 1),
                                        uvCoordinateList.get(Integer.valueOf(parts[1]) - 1),
                                        normalList.get(Integer.valueOf(parts[2]) - 1)));
                            }
                            faces.add(new Face(verts[0], verts[1], verts[2]));
                        }
                    }
                break;
            }
        }
        //To add last model to modellist
        Material m = materials.computeIfAbsent(lastMaterialName, (key) -> {
            Material lastResort = new Material();
            lastResort.textures.put(TextureType.DiffuseMap, textureBuilder.buildTexture(1.0f, 1.0f, 1.0f));
            lastResort.textures.put(TextureType.SpecularHighlightMap, textureBuilder.buildTexture(0, 0, 0));
            lastResort.textures.put(TextureType.BumpMap, textureBuilder.buildTexture(0, 0, 0));
            return lastResort;
        });
        Model model = new Model();
        model.setName(lastName);
        model.setVertices(vertexArrayBuilder.buildVertexArray(faces));
        model.setMaterial(m);
        modelList.add(model);
        System.out.println("come from");
        return modelList;
    }

    /*
    private Vertex setTangents(Vertex[] verts) {
        Vector3f q1 = Vector3f.sub(verts[1].getPos(), verts[0].getPos(), null);
        Vector3f q2 = Vector3f.sub(verts[1].getPos(), verts[0].getPos(), null);

        Vector2f uv1 = Vector2f.sub(verts[1].getTex(), verts[0].getTex(), null);
        Vector2f uv2 = Vector2f.sub(verts[2].getTex(), verts[0].getTex(), null);

        return null;
    }
    */

    private Vertex setTangents(Vertex vertex) {
        Vector3f t = new Vector3f(vertex.getNormal());
        t.add(t, new Vector3f(vertex.getTex().x, vertex.getTex().y, 0), t);
        Vector3f tangent = Vector3f.cross(t, vertex.getNormal(), null);
        Vector3f bitangent = Vector3f.cross(tangent, vertex.getNormal(), null);
        vertex.setTangent(tangent);
        vertex.setBitangent(bitangent);
        return vertex;
    }

    private List<String> loadFile(String path) {
        List<String> file = new ArrayList<>();
        try {
            try(BufferedReader mtlStream = new BufferedReader(new FileReader(path))) {
                String line;
                while ((line = mtlStream.readLine()) != null) { file.add(line); }
            }
        } catch (IOException e) {
            System.err.println("IOException while trying to read " + path);
            e.printStackTrace();
        }
        return file;
    }

    private void loadMaterials(String filename) {
        try(BufferedReader  mtlStream = new BufferedReader(new FileReader(filename))) {
            String line;
            Material currentMaterial = null;
            while ((line = mtlStream.readLine()) != null) {
                StringTokenizer st = new StringTokenizer(line.trim(), " ");
                if (!line.isEmpty()) {
                    switch (st.nextToken()) {
                        case "newmtl":
                            currentMaterial = new Material();
                            currentMaterial.name = st.nextToken();
                            materials.put(currentMaterial.name, currentMaterial);
                            break;
                        case "Ns":
                            currentMaterial.specularCoefficient = Float.parseFloat(st.nextToken());
                            break;
                        case "Ka":
                            currentMaterial.ambient = new Vector3f(
                                    Float.parseFloat(st.nextToken()),
                                    Float.parseFloat(st.nextToken()),
                                    Float.parseFloat(st.nextToken()));
                            break;
                        case "Kd":
                            currentMaterial.diffuse = new Vector3f(
                                    Float.parseFloat(st.nextToken()),
                                    Float.parseFloat(st.nextToken()),
                                    Float.parseFloat(st.nextToken()));
                            break;
                        case "Ks":
                            currentMaterial.specular = new Vector3f(
                                    Float.parseFloat(st.nextToken()),
                                    Float.parseFloat(st.nextToken()),
                                    Float.parseFloat(st.nextToken()));
                            break;
                        case "Ni":
                            currentMaterial.refractionIndex = Float.parseFloat(st.nextToken());
                            break;
                        case "d":
                            currentMaterial.transparency = Float.parseFloat(st.nextToken());
                            break;
                        //Any texture
                        case "map_Kd": case "map_Ks": case "map_Ns":
                        case "map_d": case "map_bump": case "disp": case "decal":
                            String[] name = line.trim().split(" ");
                            currentMaterial.textures.put(TextureType.getTypeFromName(name[0]), textureBuilder.buildTexture(modelDir + name[1]));
                            break;
                        default:
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("hej");
    }
    public VertexArray getVertexArray() {
        return modelList.get(0).getVertices();
    }

    public Material getMaterial() {
        return modelList.get(0).getMaterial();
    }

    public List<Model> getModelList() {
        return modelList;
    }
}


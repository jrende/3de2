package jrende.graphics.util;

import org.lwjgl.util.vector.Vector3f;

public class OctNode {
    private final Vector3f origin;
    private final int halfSize;
    OctNode[] children;
    AABB data;
    int index = -1;

    public OctNode(Vector3f origin, int halfSize) {
        this.origin = origin;
        this.halfSize = halfSize;
    }

    public void insert(AABB point) {
        for(int i = 0; i < 8; i++) {
            insert(point, i);
        }
    }

    public void insert(AABB aabb, int index) {
        if(children != null) {
            //Has children. Which one should we insert into?
            int dest = getOctantContainingPoint(aabb.points[index]);
            children[dest].insert(aabb, index);
        } else  if(data == null) {
            data = aabb;
            this.index = index;
        } else {
            //Leaf node with no data. Just set data to AABB
            //No children, but contains data. Split into 8.
            initializeChildren();
            this.insert(aabb, index);
            this.insert(data, this.index);
            data = null;
            this.index = -1;
        }
    }

    private void initializeChildren() {
        children = new OctNode[8];
        Vector3f[] points = new Vector3f[8];
        for (int i = 0; i < 8; i++) {
            points[i] = new Vector3f(origin);
            points[i].x = halfSize * (i%8==0 ? .5f : -.5f);
            points[i].y = halfSize * (i%4==0 ? .5f : -.5f);
            points[i].z = halfSize * (i%2==0 ? .5f : -.5f);
            children[i] = new OctNode(points[i], halfSize/8);
        }
    }

    int getOctantContainingPoint(Vector3f point) {
        int oct = 0;
        if(point.x >= origin.x) oct |= 4;
        if(point.y >= origin.y) oct |= 2;
        if(point.z >= origin.z) oct |= 1;
        return oct;
    }

    public int numPoints() {
        int tot = 0;
        if(children == null) {
            if(data != null) {
                tot = 1;
            }
            return tot;
        }
        for(OctNode tree: children) {
            tot += tree.numPoints();
        }
        return tot;
    }
}

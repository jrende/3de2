package jrende.graphics.util;

import org.lwjgl.util.vector.Vector3f;

public class Face {
    Vertex v1 ,v2, v3;

    public Face(Vertex v1, Vertex v2, Vertex v3) {
        this.v1 = v1;
        this.v2 = v2;
        this.v3 = v3;
    }

    public Vertex[] asArray() {
        return new Vertex[]{v1, v2, v3};
    }

    public void setNormal(Vector3f normal) {
        v1.setNormal(normal);
        v2.setNormal(normal);
        v3.setNormal(normal);
    }

    public void setTangent(Vector3f tangent) {
        v1.setTangent(tangent);
        v2.setTangent(tangent);
        v3.setTangent(tangent);
    }

    public void setBitangent(Vector3f bitangent) {
        v1.setBitangent(bitangent);
        v2.setBitangent(bitangent);
        v3.setBitangent(bitangent);
    }
}

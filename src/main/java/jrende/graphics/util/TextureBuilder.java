package jrende.graphics.util;

import jrende.graphics.texture.Texture;
import jrende.graphics.texture.Texture2D;
import org.lwjgl.util.vector.Vector3f;

import java.io.File;
import java.io.IOException;

/**
 * Created by tokjos on 2014-07-13.
 */
public class TextureBuilder {
    public Texture buildTexture(String path) {
        path = path.replace("\\", File.separator);
        path = path.replace("/", File.separator);
        Texture t;
        try {
            t = new Texture2D(path);
        } catch (IOException e) {
            System.err.println(e.getMessage());
            t = new Texture2D(1.0f, 1.0f, 1.0f);
        }
        return t;
    }

    public Texture buildTexture(float r, float g, float b) {
        return new Texture2D(r, g, b);
    }
}

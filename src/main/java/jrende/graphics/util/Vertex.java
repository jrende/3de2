package jrende.graphics.util;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public class Vertex {
	public Vector3f pos;
	public Vector2f tex;
	public Vector3f normal;
    public Vector3f tangent;
    public Vector3f bitangent;

    public Vertex(Vector3f pos) {
        this.pos = pos;
    }

    public Vertex(Vector3f pos, Vector2f tex) {
        this.pos = pos;
        this.tex = tex;
    }

	public Vertex(Vector3f pos, Vector2f tex, Vector3f normal) {
		this.pos = pos;
		this.tex = tex;
		this.normal = normal;
	}

    public Vector3f getPos() {
		return pos;
	}

	public void setPos(Vector3f pos) {
		this.pos = pos;
	}

	public Vector2f getTex() {
		return tex;
	}

	public void setTex(Vector2f tex) {
		this.tex = tex;
	}

	public Vector3f getNormal() {
		return normal;
	}

	public void setNormal(Vector3f normal) {
		this.normal = normal;
	}

    public Vector3f getTangent() {
        return tangent;
    }

    public void setTangent(Vector3f tangent) {
        this.tangent = tangent;
    }

    public Vector3f getBitangent() {
        return bitangent;
    }

    public void setBitangent(Vector3f bitangent) {
        this.bitangent = bitangent;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("(");
        if(pos != null) {
            sb.append(pos.toString());
        }
        if(tex != null) {
            sb.append(", ").append(tex.toString());
        }
        if(normal != null) {
            sb.append(", ").append(normal.toString());
        }
        return sb.append(")").toString();
    }
}
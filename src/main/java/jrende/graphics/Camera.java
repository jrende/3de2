package jrende.graphics;

import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import java.nio.FloatBuffer;


public class Camera {
    private static final float PIOVER180 = (float) (Math.PI / 180);
//	public float STEP = 0.1f;

    public Vector3f target;
    public Vector3f up;
    public Vector3f pos;

    Quaternion rot;
    Matrix4f rotMat;

    FloatBuffer rotationMatrixBuffer;

    public Camera() {
        pos = new Vector3f(0.0f, 0.0f, 0.0f);
        target = new Vector3f(1.0f, 0.0f, 0.0f);
        up = new Vector3f(0, 1.0f, 0);

        rot = new Quaternion();
        rotMat = new Matrix4f();
        rotationMatrixBuffer = BufferUtils.createFloatBuffer(4 * 4);
    }

    public Camera(Vector3f pos) {
        this.pos = pos;
        target = new Vector3f(1.0f, 0.0f, 0.0f);
        up = new Vector3f(0, 1.0f, 0);

        rot = new Quaternion();
        rotMat = new Matrix4f();
        rotationMatrixBuffer = BufferUtils.createFloatBuffer(4 * 4);
    }

    public static float ToRadian(float deg) {
        return (float) (deg * (Math.PI / 180));
    }

    public static float ToDegree(double rad) {
        return (float) (rad * (180 / Math.PI));
    }

    Vector4f newTarget = new Vector4f(1, 0, 0, 0);
    Vector4f newUp = new Vector4f(0, 1, 0, 0);

    public void update() {
        rotMat.setIdentity();
        rotMat = quatToMatrix(rot);

        newTarget.set(1, 0, 0, 0);
        Matrix4f.transform(rotMat, newTarget, newTarget);
        newTarget.normalise();
        target.x = newTarget.x;
        target.y = newTarget.y;
        target.z = newTarget.z;

        newUp.set(0, 1, 0, 0);
        Matrix4f.transform(rotMat, newUp, newUp);
        newUp.normalise();
        up.x = newUp.x;
        up.y = newUp.y;
        up.z = newUp.z;
    }

    public void onRender() {
        update();
    }

    public Vector3f getPos() {
        return pos;
    }

    public Vector3f getTarget() {
        return target;
    }

    public Vector3f getUp() {
        return up;
    }

    public void rotate(Vector4f axisAngle) {
        Quaternion q = new Quaternion();
        q.setFromAxisAngle(axisAngle);
        Quaternion.mul(rot, q, rot);
    }

    private Matrix4f quatToMatrix(Quaternion rot) {
        float qw = rot.w;
        float qx = rot.x;
        float qy = rot.y;
        float qz = rot.z;

        float n = (float) (1.0f / Math.sqrt(qx * qx + qy * qy + qz * qz + qw * qw));
        qx *= n;
        qy *= n;
        qz *= n;
        qw *= n;
        float[] f = {
                1.0f - 2.0f * qy * qy - 2.0f * qz * qz, 2.0f * qx * qy - 2.0f * qz * qw, 2.0f * qx * qz + 2.0f * qy * qw, 0.0f,
                2.0f * qx * qy + 2.0f * qz * qw, 1.0f - 2.0f * qx * qx - 2.0f * qz * qz, 2.0f * qy * qz - 2.0f * qx * qw, 0.0f,
                2.0f * qx * qz - 2.0f * qy * qw, 2.0f * qy * qz + 2.0f * qx * qw, 1.0f - 2.0f * qx * qx - 2.0f * qy * qy, 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f};
        rotationMatrixBuffer.clear();
        rotationMatrixBuffer.put(f);
        rotationMatrixBuffer.flip();
        rotMat.load(rotationMatrixBuffer);
        return rotMat;
    }
}

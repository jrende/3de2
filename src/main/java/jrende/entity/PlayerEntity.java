package jrende.entity;

import com.artemis.Entity;
import com.artemis.World;
import com.bulletphysics.linearmath.Transform;
import jrende.game.CameraComponent;
import jrende.graphics.Camera;
import jrende.graphics.TransformComponent;
import jrende.physics.CollisionComponent;
import org.lwjgl.util.vector.Vector3f;

public class PlayerEntity {
    Entity entity;
    TransformComponent modelComp;
    CameraComponent camComp;
    CollisionComponent collComp;
    boolean noclip = true;

    public PlayerEntity(Entity e) {
        this.entity = e;
    }

    public void setModelComponent(TransformComponent component) {
        entity.addComponent(component);
        this.modelComp = component;
    }

    public void setCameraComponent(CameraComponent component) {
        entity.addComponent(component);
        this.camComp = component;
    }

    public void setCollisionComponent(CollisionComponent component) {
        entity.addComponent(component);
        this.collComp = component;
    }

    Vector3f dirMF = new Vector3f();

    public void moveForward(float step) {
        Camera cam = camComp.getCamera();
        dirMF.set(cam.target.x, 0, cam.target.z);
        dirMF.normalise();
        dirMF.x *= step;
        dirMF.z *= -step;
        translate(dirMF);
    }

    Vector3f dirMB = new Vector3f();

    public void moveBackward(float step) {
        Camera cam = camComp.getCamera();
        dirMB.set(cam.target.x, 0, cam.target.z);
        dirMB.normalise();
        dirMB.x *= -step;
        dirMB.z *= step;
        translate(dirMB);
    }

    Vector3f dirML = new Vector3f();

    public void moveLeft(float step) {
        Camera cam = camComp.getCamera();
        Vector3f.cross(cam.target, cam.up, dirML);
        dirML.y = 0;
        dirML.normalise();
        dirML.x *= step;
        dirML.z *= -step;
        translate(dirML);
    }

    Vector3f dirMR = new Vector3f();

    public void moveRight(float step) {
        Camera cam = camComp.getCamera();
        Vector3f.cross(cam.target, cam.up, dirMR);
        dirMR.y = 0;
        dirMR.normalise();
        dirMR.x *= -step;
        dirMR.z *= step;
        translate(dirMR);
    }

    public void translate(Vector3f pos) {
        if (noclip) {
            camComp.getCamera().getPos().translate(pos.x, pos.y, pos.z);
        } else {
            collComp.rigidBody.activate();
            collComp.translate(pos);
        }
    }

    Transform tr = new Transform();

    public void updatePlayerCamera() {
        Camera cam = camComp.getCamera();
        if (!noclip) {
            collComp.rigidBody.getMotionState().getWorldTransform(tr);
            cam.pos = modelComp.getPos();
            //Player height
            cam.pos.y += 2.0f;
        }
    }

    public boolean isNoclip() {
        return noclip;
    }

    public void setNoclip(boolean noclip) {
        this.noclip = noclip;
    }

    public Camera getCamera() {
        return camComp.getCamera();
    }
}
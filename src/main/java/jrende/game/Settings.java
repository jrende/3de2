package jrende.game;

public class Settings {
    final static int width = 1024;
    final static int height = 768;
    final static boolean fullscreen = false;
    final static boolean debug = true;


    public static int getHeight() {
        return height;
    }

    public static int getWidth() {
        return width;
    }

    public static boolean getFullscreen() {
        return fullscreen;
    }

    public static boolean getDebug() {
        return debug;
    }
}

package jrende.game;

import com.artemis.Entity;
import com.artemis.World;
import com.bulletphysics.collision.shapes.CollisionShape;
import jrende.entity.PlayerEntity;
import jrende.graphics.Camera;
import jrende.graphics.RenderComponent;
import jrende.graphics.TransformComponent;
import jrende.graphics.g3d.light.DirectionalLightComponent;
import jrende.graphics.g3d.light.LightComponent;
import jrende.graphics.g3d.light.PointLightComponent;
import jrende.graphics.g3d.light.SpotLightComponent;
import jrende.physics.CollisionComponent;
import org.lwjgl.util.vector.Vector3f;

public class EntityUtil {
    public static LightComponent createDirLight(Vector3f color, Vector3f dir) {
        DirectionalLightComponent plc = new DirectionalLightComponent();
        color.normalise();
        plc.setAmbientIntensity(0.125f);
        plc.setColor(color);
        plc.setDiffuseIntensity(0.0f);
        plc.setDirection(dir);
        return plc;
    }

    public static SpotLightComponent createSpotLight(Vector3f color, Vector3f position, Vector3f dir) {
        dir.normalise();
        SpotLightComponent plc = new SpotLightComponent();
        plc.setAmbientIntensity(0.0f);
        plc.setColor(color);
        plc.setConstant(0.5f);
        plc.setExp(0.1f);
        plc.setLinear(0.1f);
        plc.setDiffuseIntensity(1.0f);
        plc.setConcentration(10);
        plc.setPosition(position);
        plc.setDir(dir);
        return plc;
    }

    public static LightComponent createLight(Vector3f color, Vector3f position) {
        PointLightComponent plc = new PointLightComponent();
        plc.setAmbientIntensity(0.0f);
        plc.setColor(color);
        plc.setDiffuseIntensity(4.0f);
        plc.setPosition(position);
        plc.setConstant(0.5f);
        plc.setExp(0.1f);
        plc.setLinear(0.1f);
        return plc;
    }

    public static LightComponent createRandomLight(Vector3f offset, float scale) {
        PointLightComponent plc = new PointLightComponent();
        plc.setAmbientIntensity((float) Math.random());
        plc.setColor(new Vector3f((float) Math.random(), (float) Math.random(), (float) Math.random()));
        plc.setDiffuseIntensity((float) Math.random());
        Vector3f rand = new Vector3f((float) Math.random()*scale, 0, (float) Math.random()*scale);
        plc.setPosition(Vector3f.add(rand, offset, null));
        plc.setConstant((float) Math.random());
        plc.setExp((float) Math.random());
        plc.setLinear((float) Math.random());
        return plc;
    }

    public static Entity createModel(World world, RenderComponent normalCube, Vector3f position, float scale) {
        Entity e = world.createEntity();
        e.addComponent(normalCube);
        TransformComponent component = new TransformComponent();
        component.translate(position);
        component.scale(new Vector3f(scale, scale, scale));
        e.addComponent(component);
        return e;
    }

    public static Entity createModel(World world, RenderComponent normalCube, CollisionShape boxShape, javax.vecmath.Vector3f position) {
        Entity e = world.createEntity();
        e.addComponent(normalCube);
        CollisionComponent collisionComponent = new CollisionComponent(boxShape, 1.0f);
        collisionComponent.translate(position);
        e.addComponent(collisionComponent);
        TransformComponent component = new TransformComponent();
        e.addComponent(component);
        return e;
    }

    public static PlayerEntity createPlayer(World world) {
        Camera cam = new Camera();
        cam.getPos().set(0, 5, 0);
        cam.getUp().set(0, -1, 0);
        cam.getTarget().set(0, 0, 1);
        Entity player = world.createEntity();
        PlayerEntity playerEntity = new PlayerEntity(player);
        TransformComponent playerModel = new TransformComponent();
        playerEntity.setModelComponent(playerModel);
        playerEntity.setCameraComponent(new CameraComponent(cam));
        world.addEntity(player);
        return playerEntity;
    }

}

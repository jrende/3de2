package jrende.game;

import com.artemis.Entity;
import com.artemis.World;
import jrende.entity.PlayerEntity;
import jrende.graphics.*;
import jrende.graphics.g3d.light.LightComponent;
import jrende.graphics.g3d.light.SpotLightComponent;
import jrende.graphics.g3d.model.Model;
import jrende.graphics.g3d.model.Scene;
import jrende.graphics.util.ObjLoader;
import jrende.physics.CollisionComponent;
import jrende.physics.PhysicsSystem;
import jrende.util.InputUtil;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.*;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import javax.vecmath.Vector2f;
import java.util.List;

//import javax.vecmath.Vector3f;


public class Game {
    private static final int FRAMES_PER_SECOND = 60;
    //int SCRWIDTH = 1920;
    //int SCRHEIGHT = 1080;
    private int depthBufferBits = 24;

    boolean cullingBackFace = true;
    boolean cullFace = true;
    boolean wireframe = false;
    Renderer renderer;

    public PlayerEntity playerEntity;
    World world;
    private PhysicsSystem physicsSystem;
    private float step = 0.1f;

    public static void main(String args[]) {
        Game g = new Game();
        g.run();
    }

    public Game() {
        createDisplay();
        setupWorld();
        createEntities();
        System.out.println("done");
    }

    private void createDisplay() {
        try {
            DisplayMode selectedMode = null;
            int width = Settings.getWidth();
            int height = Settings.getHeight();
            for (DisplayMode mode : Display.getAvailableDisplayModes()) {
                if (mode.getHeight() ==  height && mode.getWidth() ==  width) {
                    selectedMode = mode;
                }
            }
            if (selectedMode == null) {
                selectedMode = new DisplayMode( width,  height);
            }
            Display.setDisplayMode(selectedMode);
            Display.setFullscreen(Settings.getFullscreen());
            PixelFormat pixelFormat = new PixelFormat(0, depthBufferBits, 0, 8);
            ContextAttribs contextAttribs = new ContextAttribs(4, 2).withProfileCore(true).withForwardCompatible(true).withProfileCompatibility(false);
            Display.create(pixelFormat, contextAttribs);
            //Display.create(pixelFormat);

        } catch (LWJGLException e) {
            System.err.println("Error creating display.");
            e.printStackTrace();
        }
        lastFPS = getTime(); // call before loop to initialise fps timer
    }

    private void setupWorld() {
        world = new World();
        renderer = new Renderer();

        physicsSystem = world.setSystem(new PhysicsSystem());
        world.setSystem(renderer);
        world.initialize();
    }

    TransformComponent planeMC;
    CollisionComponent collisionComponent;

    SpotLightComponent[] l = new SpotLightComponent[4];
    private void createEntities() {
        //CollisionShape boxShape = new BoxShape(new javax.vecmath.Vector3f(1, 1, 1));
        playerEntity = EntityUtil.createPlayer(world);
        renderer.setCamera(playerEntity.getCamera());
        //renderer.addLight(EntityUtil.createDirLight(new Vector3f(1, 1, 1), new Vector3f(-1, -1, -1)));
        //renderer.addLight(EntityUtil.createLight(new Vector3f(1, 1, 1), new Vector3f(-129.89877f, 18.500032f, -8.299997f)));
        //renderer.addLight(EntityUtil.createLight(new Vector3f(1, 1, 1), new Vector3f(0, 0, 0)));
        ObjLoader obj = new ObjLoader("sponza.obj");

        TransformComponent sponzaTransform = new TransformComponent();
        sponzaTransform.scale(new Vector3f(0.1f, 0.1f, 0.1f));
        for(int i = -5; i < 5; i++) {
            renderer.addLight(EntityUtil.createLight(new Vector3f(1, 1, 1), new Vector3f(i*30, 10, 10)));
        }
        for(int i = -5; i < 5; i++) {
            renderer.addLight(EntityUtil.createLight(new Vector3f(1, 1, 1), new Vector3f(i*30, 10, -20)));
        }

        for(Model model: obj.getModelList()) {
            Entity e = world.createEntity();
            e.addComponent(new RenderComponent(model));
            e.addComponent(sponzaTransform);
            world.addEntity(e);
        }
        /*
        RenderComponent floorModel = new RenderComponent(new Model("Floor.obj"));
        //RenderComponent cubeModel = new RenderComponent(new Model("QuadCube.obj"));
        l[0] = EntityUtil.createSpotLight(new Vector3f(1, 1, 1), new Vector3f(2, 0.5f, 2), new Vector3f(0, 0, 1));
        l[1] = EntityUtil.createSpotLight(new Vector3f(1, 1, 1), new Vector3f(2, 0.5f, 2), new Vector3f(0, 0, -1));
        l[2] = EntityUtil.createSpotLight(new Vector3f(1, 1, 1), new Vector3f(2, 0.5f, 2), new Vector3f(1, 0, 0));
        l[3] = EntityUtil.createSpotLight(new Vector3f(1, 1, 1), new Vector3f(2, 0.5f, 2), new Vector3f(-1, 0, 0));
        for(SpotLightComponent light: l) {
            renderer.addLight(light);
        }
        //renderer.addLight(createLight(new Vector3f(1,1,1), new Vector3f(0, -10, 0)));
        //Entity floor = createModel(floorModel, new Vector3f(0, 0, 0), 2.0f);
        //world.addEntity(floor);
        //Entity cube = createModel(cubeModel, new Vector3f(0, -10, 0), 10);
        //world.addEntity(cube);
        for(int i = -5; i < 5; i++) {
            for(int j = -5; j < 5; j++) {
                //Entity floor = createModel(normalCube, boxShape, new javax.vecmath.Vector3f(i*15, 0, j*15));
                if((i % 2 == 0) || (j % 2 == 0)) {
                    Entity cube = EntityUtil.createModel(world, cubeModel, new Vector3f(i * 4, 1.1f, j * 4), 1);
                    world.addEntity(cube);
                }
                if((i % 3 == 0) && (j % 3 == 0)) {
                    float r = (float) (0.5f * Math.random()/2);
                    float g = (float) (0.5f * Math.random()/2);
                    float b = (float) (0.5f * Math.random()/2);
                    renderer.addLight(EntityUtil.createLight(new Vector3f(r, g, b), new Vector3f(i * 4 + 2, 3, j * 4 + 2)));
                }
                Entity floor = EntityUtil.createModel(world, floorModel, new Vector3f(i*4, 0, j*4), 2.0f);
                world.addEntity(floor);
            }
        }
        //renderer.addLight(createLight(new Vector3f(1, 1, 1), new Vector3f(0, 1, 0)));
        //renderer.addLight(createLight(new Vector3f(1, 0, 0), new Vector3f(0, 1, 0)));
        //renderer.addLight(createLight(new Vector3f(0, 1, 0), new Vector3f(0, 1, 0)));
        //renderer.addLight(createLight(new Vector3f(0, 0, 1), new Vector3f(0, 1, 0)));

        //this.playerEntity = createPlayer();
        //cam.rotate();
        //Matrix4f.rotate(0.05f, new Vector3f(1, 0, 0), rotMat, rotMat);
        Matrix4f.rotate(0.02f, new Vector3f(0, 1, 0), rotMat, rotMat);
        //Matrix4f.rotate(0.07f, new Vector3f(0, 0, 1), rotMat, rotMat);
        */
    }
    float timestep = 0;
    public long getTime() {
        return (Sys.getTime() * 1000) / Sys.getTimerResolution();
    }

    long lastFPS;
    int fps;

    long time = getTime();
    Matrix4f rotMat = new Matrix4f();
    Vector4f temp = new Vector4f();

    public void run() {
        while (!Display.isCloseRequested() && floop) {
            if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
                Display.destroy();
                break;
            }
            input();

            /*
            for(SpotLightComponent light: l) {
                temp.setX(light.getDir().x);
                temp.setY(light.getDir().y);
                temp.setZ(light.getDir().z);
                temp.setW(1.0f);
                Matrix4f.transform(rotMat, temp, temp);
                light.getDir().setX(temp.x);
                light.getDir().setY(temp.y);
                light.getDir().setZ(temp.z);
            }
            */
            //rotMat.rotate((float) (Math.sin(time / 1000) * Math.PI), new Vector3f(0.2f, 0.4f, 1.0f), rotMat);
            world.process();
            updateFPS();
        }
    }
    private void updateFPS() {
        long now = getTime();
        world.setDelta(now - time);
        time = now;
        Display.sync(FRAMES_PER_SECOND);
        if (getTime() - lastFPS > 1000) {
            Display.setTitle("FPS: " + fps);
            fps = 0;
            lastFPS += 1000;
        }
        fps++;
    }

    boolean timeStepSwitch = true;
    boolean grabMouse = false;

    int currentLight = 0;
    private void input() {
        while (Keyboard.next()) {
            if (Keyboard.getEventKeyState()) {
                if (Keyboard.getEventKey() == Keyboard.KEY_C) {
                    cullFace = !cullFace;
                    if (cullFace) {
                        System.out.println("Cull back faces");
                        renderer.setCullFace(GL11.GL_BACK);
                    } else {
                        System.out.println("Cull front faces");
                        renderer.setCullFace(GL11.GL_FRONT);
                    }
                }
                if (Keyboard.getEventKey() == Keyboard.KEY_T) {
                    timeStepSwitch = !timeStepSwitch;
                    if (timeStepSwitch) {
                        timestep = 0;
                    } else {
                        timestep = 1.0f / FRAMES_PER_SECOND;
                    }
                    physicsSystem.setTimeStep(timestep);
                }
                if (Keyboard.getEventKey() == Keyboard.KEY_Y) {
                    playerEntity.setNoclip(!playerEntity.isNoclip());
                }
                if (Keyboard.getEventKey() == Keyboard.KEY_G) {
                    grabMouse = !grabMouse;
                    Mouse.setGrabbed(grabMouse);
                }
                if (Keyboard.getEventKey() == Keyboard.KEY_X) {
                    cullingBackFace = !cullingBackFace;
                    System.out.println("Back face culling: " + cullingBackFace);
                    renderer.enableBackfaceCulling(cullingBackFace);

                }

                if(Keyboard.isKeyDown(Keyboard.KEY_RCONTROL)) {
                    renderer.drawAllLights();
                }
                if (Keyboard.isKeyDown(Keyboard.KEY_NEXT)) {
                    if(currentLight < renderer.getLights().size()) {
                        currentLight++;
                    }
                    renderer.drawOnlyLight(currentLight);
                }
                if (Keyboard.isKeyDown(Keyboard.KEY_PRIOR)) {
                    if(currentLight > 0) {
                        currentLight--;
                    }
                    renderer.drawOnlyLight(currentLight);
                }
                if (Keyboard.isKeyDown(Keyboard.KEY_Z)) {
                    wireframe = !wireframe;
                    System.out.println("Render wireframe");
                    renderer.enableWireframe(wireframe);
                }

                if (Keyboard.isKeyDown(Keyboard.KEY_MULTIPLY)) {
                    renderer.fov += 2.5;
                }

                if (Keyboard.isKeyDown(Keyboard.KEY_DIVIDE)) {
                    renderer.fov -= 2.5;
                }
            } else {

            }
        }
            if (Keyboard.isKeyDown(Keyboard.KEY_B)) {
            collisionComponent.addRotForce(new javax.vecmath.Vector3f(0, 0, 1));
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_END)) {
            renderer.setFOV(renderer.getFOV() - 1.0f);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_HOME)) {
            renderer.setFOV(renderer.getFOV() + 1.0f);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
            playerEntity.moveForward(step);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
            playerEntity.moveBackward(step);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
            playerEntity.moveLeft(step);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
            playerEntity.moveRight(step);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
            playerEntity.translate(downVec);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
            playerEntity.translate(upVec);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_RSHIFT)) {
            float f = .1f;
            if (Keyboard.isKeyDown(Keyboard.KEY_I)) {
                planeMC.translate(new Vector3f(f, 0.0f, 0.0f));
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_J)) {
                planeMC.translate(new Vector3f(0.0f, 0.0f, f));
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_K)) {
                planeMC.translate(new Vector3f(-f, 0.0f, 0.0f));
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_L)) {
                planeMC.translate(new Vector3f(0.0f, 0.0f, -f));
            }
        } else {
            float f = .1f;
            LightComponent onlyLight = renderer.getOnlyLight(currentLight);
            if(onlyLight != null) {
                if (Keyboard.isKeyDown(Keyboard.KEY_I)) {
                    onlyLight.getPosition().translate(f, 0, 0);
                }
                if (Keyboard.isKeyDown(Keyboard.KEY_J)) {
                    onlyLight.getPosition().translate(0, 0, f);
                }
                if (Keyboard.isKeyDown(Keyboard.KEY_K)) {
                    onlyLight.getPosition().translate(-f, 0, 0);
                }
                if (Keyboard.isKeyDown(Keyboard.KEY_L)) {
                    onlyLight.getPosition().translate(0, 0, -f);
                }
                if (Keyboard.isKeyDown(Keyboard.KEY_O)) {
                    onlyLight.getPosition().translate(0, f, 0);
                }
                if (Keyboard.isKeyDown(Keyboard.KEY_U)) {
                    onlyLight.getPosition().translate(0, -f, 0);
                }
                if (Keyboard.isKeyDown(Keyboard.KEY_P)) {
                    System.out.println(renderer.getCurrentLight().getPosition());
                }

                int wheel = Mouse.getDWheel();
                if (wheel > 0) {
                    step *= 1.5;
                    downVec.scale(1.5f);
                    upVec.scale(1.5f);
                }
                if (wheel < 0) {
                    step /= 1.5;
                    downVec.scale(0.6667f);
                    upVec.scale(0.6667f);
                }
            }
        }
        mouseVec.set(Mouse.getDX(), Mouse.getDY());
        InputUtil.moveCameraFromMouse(renderer.getActiveCamera(), mouseVec);
        playerEntity.updatePlayerCamera();
    }

    Vector2f mouseVec = new Vector2f();
    Vector3f downVec = new Vector3f(0, step, 0);
    Vector3f upVec = new Vector3f(0, -step, 0);

    boolean floop = true;
}

package jrende.game;

import jrende.graphics.Camera;

import com.artemis.Component;

public class CameraComponent extends Component {
	Camera camera;
	public CameraComponent(Camera camera) {
		this.camera = camera;
	}
	
	public Camera getCamera() {
		return camera;
	}
	
}

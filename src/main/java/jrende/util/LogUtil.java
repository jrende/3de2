package jrende.util;

import org.lwjgl.opengl.OpenGLException;
import org.lwjgl.opengl.Util;

public class LogUtil {
    public static void getGlError() {
        try {
            Util.checkGLError();
        } catch (OpenGLException e) {
            e.printStackTrace();
            System.exit(1);
        } finally {
            System.out.println("No error");
        }

    }
}

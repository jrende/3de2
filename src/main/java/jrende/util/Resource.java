package jrende.util;

public interface Resource {
	void free();
}

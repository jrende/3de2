package jrende.util;

import jrende.graphics.Camera;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import javax.vecmath.Vector2f;

public class InputUtil {
    static Vector4f camRot = new Vector4f();

    public static void moveCameraFromMouse(Camera cam, Vector2f mouse) {
        Vector3f up = cam.getUp();
        camRot.set(up.x, up.y, up.z, -mouse.x / 100.0f);
        cam.rotate(camRot);
        camRot.set(1, 0, 0, mouse.y / 100.0f);
        cam.rotate(camRot);
    }


}

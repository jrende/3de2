package jrende.util;

import java.util.ArrayList;
import java.util.List;

public class ResourceManager {
	List<Resource> rList;
	private static ResourceManager instance = new ResourceManager();
	private ResourceManager() {
		rList = new ArrayList<Resource>();
	}
	public static ResourceManager getInstance() {
		return instance;
	}
	public void addResource(Resource r) {
		rList.add(r);
	}
	public void freeResources() {
		for (int i = 0; i < rList.size(); i++) {
			rList.get(i).free();
		}
	}
}

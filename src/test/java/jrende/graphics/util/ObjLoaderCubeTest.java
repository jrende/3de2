package jrende.graphics.util;

import jrende.graphics.texture.TextureType;
import org.junit.BeforeClass;
import org.junit.Test;
import org.lwjgl.util.vector.Vector3f;
import org.mockito.ArgumentCaptor;

import java.util.List;

import static org.mockito.Mockito.*;

import static org.junit.Assert.*;

public class ObjLoaderCubeTest {
    static ObjLoader loader;
    static TextureBuilder tb;
    static VertexArrayBuilder vab;
    static List<Face> faces;

    @BeforeClass
    public static void beforeClass() {
        tb = mock(TextureBuilder.class);
        vab = mock(VertexArrayBuilder.class);
        loader = new ObjLoader("Cube.obj", vab, tb);
        ArgumentCaptor<List> list = ArgumentCaptor.forClass(List.class);
        verify(vab).buildVertexArray(list.capture());
        faces = list.getValue();
    }

    @Test
    public void testCubeShouldHave12Faces() throws Exception {
        assertEquals(12, faces.size());
    }

    @Test
    public void testFirstNormalShouldBeNegativeOneX() throws Exception {
        Vector3f normal = faces.get(0).v1.getNormal();
        assertEquals("normal x", -1, normal.getX(), .01);
        assertEquals("normal y",  0, normal.getY(), 0.1);
        assertEquals("normal z",  0, normal.getZ(), 0.1);
    }

    @Test
    public void testSecondNormalShouldAlsoBeNegativeOneX() throws Exception {
        Vector3f normal = faces.get(1).v1.getNormal();
        assertEquals("2nd normal x", -1, normal.getX(), .01);
        assertEquals("2nd normal y",  0, normal.getY(), 0.1);
        assertEquals("2nd normal z",  0, normal.getZ(), 0.1);
    }

    @Test
    public void testFirstTangentShouldBeOneZ() throws Exception {
        Vector3f tangent = faces.get(0).v1.getTangent();
        assertEquals("tangent x", 0, tangent.getX(), 0.1);
        assertEquals("tangent y", 0, tangent.getY(), 0.1);
        assertEquals("tangent z", 1, tangent.getZ(), 0.1);
    }

    @Test
    public void testFirstBiTangentShouldBeNegativeOneY() throws Exception {
        Vector3f bitangent = faces.get(0).v1.getBitangent();
        assertEquals("bitangent x",  0, bitangent.getX(), 0.001);
        assertEquals("bitangent y", -1, bitangent.getY(), 0.1);
        assertEquals("bitangent z",  0, bitangent.getZ(), 0.001);
    }

    @Test
    public void testCubeDiffuseTextureShouldBeStoneDiffuse() throws Exception {
        verify(tb).buildTexture("gfx/StoneDiffuse.png");
    }
}
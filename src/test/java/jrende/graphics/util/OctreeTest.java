package jrende.graphics.util;

import junit.framework.TestCase;
import org.junit.Assert;
import org.lwjgl.util.vector.Vector3f;

public class OctreeTest extends TestCase {

    public void setUp() throws Exception {
        super.setUp();

    }

    public void tearDown() throws Exception {

    }

    public void test_whenAddOneAABB_sizeShouldBeOne() throws Exception {
        Octree octree = new Octree(new Vector3f(0, 0, 0), 4096);
        AABB cube1 = new AABB(new Vector3f(0, 0, 0), 1, 1, 1);
        octree.insert(cube1);
        Assert.assertEquals(1, octree.size());
    }
}